﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace KurGames.Mapuches.Common.Utils
{
    public enum SpriteColorState
    {
        None,
        Normal,
        Highlighted,
        Disabled
    }

    public class SpriteColorChanger : MonoBehaviour
    {
        [SerializeField] private SpriteRenderer spriteRenderer;
        [SerializeField] private Color normalColor = Color.white;
        [SerializeField] private Color highlightColor = Color.white;
        [SerializeField] private Color disabledColor = Color.gray;
        [SerializeField] private Color noneColor = Color.black;

        private Color currentColor;

        private void Awake()
        {
            currentColor = spriteRenderer.color;
        }

        public Color GetColorByState(SpriteColorState state)
        {
            switch (state)
            {
                case SpriteColorState.Normal:
                    return normalColor;
                case SpriteColorState.Highlighted:
                    return highlightColor;
                case SpriteColorState.Disabled:
                    return disabledColor;
                default:
                    return noneColor;
            }
        }

        public void SetState(SpriteColorState state, float duration = 0)
        {
            StopAllCoroutines();

            var prevColor = currentColor;
            SetColor(GetColorByState(state));
            if (duration <= 0)
            {
                return;
            }
            StartCoroutine(BackToColor(prevColor, duration));
        }

        private IEnumerator BackToColor(Color color, float waitTime)
        {
            yield return new WaitForSeconds(waitTime);
            SetColor(color);
        }

        public void SetColor(Color color)
        {
            currentColor = color;
            spriteRenderer.color = currentColor;
        }
    }
}
