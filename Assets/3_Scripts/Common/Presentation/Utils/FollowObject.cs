﻿using UnityEngine;

namespace KurGames.Mapuches.Common.Utils
{
    public class FollowObject : MonoBehaviour
    {
        [SerializeField] private Transform target = default;
        [SerializeField] private bool freezeZ = default;

        private void Update()
        {
            if (target == null)
            {
                return;
            }
            if (freezeZ)
            {
                transform.position = new Vector3(target.position.x, target.position.y, transform.position.z);
                return;
            }
            transform.position = target.position;
        }
    }
}
