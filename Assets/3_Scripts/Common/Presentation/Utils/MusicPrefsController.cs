﻿using UnityEngine;

namespace KurGames.Mapuches.Common.Utils
{
    public class MusicPrefsController : MonoBehaviour
    {
        [SerializeField] private AudioSource audioSource = default;
        public float DefaultMusicVolume = 0.3f;

        private const string IS_MUSIC_ON = "IS_MUSIC_ON";

        private void Awake()
        {
            if (audioSource == null)
            {
                return;
            }
            DefaultMusicVolume = audioSource.volume;

            if (!IsMusicOn())
            {
                SetMusicOff();
            }
            else
            {
                SetMusicOn();
            }
        }

        public bool IsMusicOn()
        {
            return bool.Parse(PlayerPrefs.GetString(IS_MUSIC_ON, bool.TrueString));
        }

        public void SaveMusicSetting(bool setOn)
        {
            PlayerPrefs.SetString(IS_MUSIC_ON, setOn ? bool.TrueString : bool.FalseString);
        }

        public void SetMusicOff()
        {
            if (audioSource == null)
            {
                return;
            }
            audioSource.volume = 0;
        }
        public void SetMusicOn()
        {
            if (audioSource == null)
            {
                return;
            }
            audioSource.volume = DefaultMusicVolume;
        }
    }
}
