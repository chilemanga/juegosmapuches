﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace KurGames.Mapuches.Common.Utils
{
    public class LoadScene : MonoBehaviour
    {
        public void Load(int sceneIndex)
        {
            SceneManager.LoadScene(sceneIndex);
        }
    }
}
