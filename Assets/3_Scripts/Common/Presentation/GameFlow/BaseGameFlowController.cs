using System;
using System.Collections;
using UnityEngine;

namespace KurGames.Mapuches.Common.Presentation.GameFlow
{
    public abstract class BaseGameFlowController : MonoBehaviour
    {
        protected virtual void Awake()
        {
            Application.targetFrameRate = 30;
        }

        public abstract void StartGame(int id = 1);
    }
}
