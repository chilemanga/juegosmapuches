using UnityEngine;

namespace KurGames.Mapuches.Common.Presentation.GameFlow
{
    public class PauseGame : MonoBehaviour
    {
        [SerializeField] private bool pauseOnEnable = true;
        [SerializeField] private bool unpauseOnDisable = true;

        private void OnEnable()
        {
            if (pauseOnEnable)
            {
                Pause();
            }
        }
        private void OnDisable()
        {
            if (unpauseOnDisable)
            {
                UnPause();
            }
        }

        public void Pause()
        {
            Time.timeScale = 0;
        }
        public void UnPause()
        {
            Time.timeScale = 1f;
        }
    }
}
