﻿namespace KurGames.Mapuches.Common.Presentation.UI
{
    public enum PopupType
    {
        Undefined,
        Instructions,
        Difficulty,
        YouWin,
        YouLose,
        AreYouSureDialog,
        ChooseGameplay,
        SettingsMenu
    }
}
