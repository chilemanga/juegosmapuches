﻿using UnityEngine;

namespace KurGames.Mapuches.Common.UI
{
    public class ExitApplicationController : MonoBehaviour
    {
        public void ExitApp()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
        }
    }
}
