﻿using KurGames.Mapuches.Common.Presentation.GameFlow;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace KurGames.Mapuches.Common.Presentation.UI
{
    public class MainUIController : MonoBehaviour
    {
        [SerializeField] private Animator animator = default;
        [SerializeField] private PlayButtonController playButtonController = default;

        [SerializeField] private BaseGameFlowController gameFlowController = null;

        private void OnEnable()
        {
            playButtonController.OnPlayButtonTapped += StartGame;
            playButtonController.OnPlayAs += StartGameAs;
        }

        private void OnDisable()
        {
            playButtonController.OnPlayButtonTapped -= StartGame;
            playButtonController.OnPlayAs = StartGameAs;
        }

        private void StartGame()
        {
            // TODO: We should use a GameController or something
            animator.SetTrigger("Outro");

            gameFlowController.StartGame(default);
        }

        private void StartGameAs(int id)
        {
            animator.SetTrigger("Outro");

            gameFlowController.StartGame(id);
        }
    }
}

