﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace KurGames.Mapuches.Common.Presentation.UI
{
    public abstract class BasePopupManager : MonoBehaviour
    {
        [SerializeField] protected List<PopupController> popupsList = default;

        protected Dictionary<PopupType, PopupController> popupsByType = new Dictionary<PopupType, PopupController>();

        protected virtual void Awake()
        {
            foreach (var popup in popupsList)
            {
                if (popupsByType.ContainsKey(popup.popupType))
                {
                    Debug.LogError($"Can't add popup of type {popup.popupType} again to the list. Check each popup config.");
                    continue;
                }
                popupsByType.Add(popup.popupType, popup);
            }
        }

        protected virtual void OnGameFinish(bool humanPlayerWin)
        {
            // When finishing game, just reload the first scene
            Action onClosePopupAction = () =>
            {
                SceneManager.LoadScene(0);
            };

            if (humanPlayerWin)
            {
                var popup = OpenPopup(PopupType.YouWin);
                popup.SetOnCloseCallback(onClosePopupAction);
            }
            else
            {
                var popup = OpenPopup(PopupType.YouLose);
                popup.SetOnCloseCallback(onClosePopupAction);
            }
        }

        protected PopupController GetPopup(PopupType popupType)
        {
            if (popupsByType.ContainsKey(popupType))
            {
                return popupsByType[popupType];
            }
            return null;
        }

        public void OpenInstructionsPopup()
        {
            OpenPopup(PopupType.Instructions);
        }
        public void OpenDifficultyPopup()
        {
            OpenPopup(PopupType.Difficulty);
        }
        public void OpenChooseGameplayPopup()
        {
            OpenPopup(PopupType.ChooseGameplay);
        }
        public void OpenSettingsPopup()
        {
            OpenPopup(PopupType.SettingsMenu);
        }

        public PopupController OpenPopup(PopupType popupType, Action cb = null)
        {
            var popup = GetPopup(popupType);
            if (popup == null)
            {
                Debug.LogError($"Can't open popup of type {popupType}. Check list of available popups.");
                return null;
            }
            popup.OpenPopupWithCallback(cb);
            return popup;
        }

        public void ClosePopup(PopupType popupType, Action cb = null)
        {
            var popup = GetPopup(popupType);
            if (popup == null)
            {
                Debug.LogError($"Can't close popup of type {popupType}. Check list of available popups.");
                return;
            }
            popup.ClosePopupWithCallback(cb);
        }

        private void OnDestroy()
        {
            popupsByType.Clear();
        }
    }
}