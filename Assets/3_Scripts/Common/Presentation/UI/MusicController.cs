﻿using KurGames.Mapuches.Common.Utils;
using UnityEngine;

namespace KurGames.Mapuches.Common.UI
{
    public class MusicController : MonoBehaviour
    {
        [SerializeField] private GameObject musicObjectOn = default;
        [SerializeField] private GameObject musicObjectOff = default;

        private AudioSource audioSource;
        public AudioSource MusicAudioSource
        {
            get
            {
                if (audioSource == null)
                {
                    audioSource = FindObjectOfType<AudioSource>();
                }
                return audioSource;
            }
        }
        private MusicPrefsController musicPrefs;
        public MusicPrefsController MusicPrefs
        {
            get
            {
                if (musicPrefs == null)
                {
                    musicPrefs = FindObjectOfType<MusicPrefsController>();
                }
                return musicPrefs;
            }
        }

        private void Start()
        {
            bool isMusicOn = MusicPrefs.IsMusicOn();
            SetMusicUIAs(isMusicOn);
        }

        public void SetMusicVolume(float volume)
        {
            if (MusicAudioSource == null)
            {
                return;
            }
            MusicAudioSource.volume = Mathf.Clamp01(volume);
        }
        public void SetMusicOff()
        {
            SetMusicVolume(0);
            SetMusicUIAs(false);
            SaveMusicSetting(false);
        }
        public void SetMusicOn()
        {
            SetMusicVolume(MusicPrefs.DefaultMusicVolume);
            SetMusicUIAs(true);
            SaveMusicSetting(true);
        }
        private void SetMusicUIAs(bool on)
        {
            musicObjectOn.SetActive(on);
            musicObjectOff.SetActive(!on);
        }
        private void SaveMusicSetting(bool musicOn)
        {
            if (MusicPrefs != null)
            {
                MusicPrefs.SaveMusicSetting(musicOn);
            }
        }
    }
}
