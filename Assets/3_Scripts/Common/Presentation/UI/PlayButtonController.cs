﻿using System;
using UnityEngine;

namespace KurGames.Mapuches.Common.Presentation.UI
{
    public class PlayButtonController : MonoBehaviour
    {
        public Action OnPlayButtonTapped = default;
        /// <summary>
        /// Use this to differentiate type of players by each game.
        /// </summary>
        public Action<int> OnPlayAs = default;

        public void OnPlayButtonTap()
        {
            OnPlayButtonTapped?.Invoke();
        }

        public void OnTapPlayAs(int id)
        {
            OnPlayAs?.Invoke(id);
        }
    }
}

