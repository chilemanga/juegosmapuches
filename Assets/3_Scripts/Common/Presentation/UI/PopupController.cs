﻿using System;
using UnityEngine;

namespace KurGames.Mapuches.Common.Presentation.UI
{
    [RequireComponent(typeof(Animator))]
    public class PopupController : MonoBehaviour
    {
        public PopupType popupType;

        private Animator animator;
        private Action onOpenPopupFinish = null;
        private Action onClosePopupFinish = null;

        private const string kOpenAnimationName = "PopupOpen";
        private const string kCloseAnimationName = "PopupClose";

        private void Awake()
        {
            animator = GetComponent<Animator>();
        }

        public void OpenPopup()
        {
            OpenPopupWithCallback(onOpenPopupFinish);
        }
        public void OpenPopupWithCallback(Action cb = null)
        {
            animator.SetTrigger(kOpenAnimationName);
            onOpenPopupFinish = cb;
        }
        // Triggered by the PopupOpen animation
        public void OnOpenPopupFinish()
        {
            onOpenPopupFinish?.Invoke();
            onOpenPopupFinish = null;
        }

        public void SetOnCloseCallback(Action onCloseCallback)
        {
            onClosePopupFinish = onCloseCallback;
        }
        public void ClosePopup()
        {
            ClosePopupWithCallback(onClosePopupFinish);
        }
        public void ClosePopupWithCallback(Action cb = null)
        {
            animator.SetTrigger(kCloseAnimationName);
            onClosePopupFinish = cb;
        }
        // Triggered by the PopupClose animation
        public void OnClosePopupFinish()
        {
            onClosePopupFinish?.Invoke();
            onClosePopupFinish = null;
        }
    }
}