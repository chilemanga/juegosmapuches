using System;
using System.Collections.Generic;
using UnityEngine;

namespace _3_Scripts.Common.Infrastructure.FSM
{
    public class StackFSM
    {
        private readonly Stack<Action> stack = new Stack<Action>();

        public void Tick()
        {
            GetCurrentState()?.Invoke();
        }

        public void PopState()
        {
            if (stack.Count == 0)
            {
                Debug.LogWarning("Pop; Stack is empty!");
                return;
            }

            stack.Pop();
        }

        public void PushState(Action state)
        {
            Debug.Log($"FSM:Push [{state.Method.Name}]");
            stack.Push(state);
        }

        private Action GetCurrentState()
        {
            return stack.Count == 0 ? null : stack.Peek();
        }

        public void Clean()
        {
            stack.Clear();
        }

    }
}
