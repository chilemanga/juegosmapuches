using UnityEngine;

namespace _3_Scripts.Common.Infrastructure.Steering
{
    public interface IBoid
    {
        Vector3 Velocity { get; }
        float MaxSpeed { get; }
        Vector3 Position { get; }
    }
}
