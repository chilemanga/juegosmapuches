using System.Collections.Generic;
using _3_Scripts.Palin.Presentation.PalinPlayers;
using UnityEngine;

namespace _3_Scripts.Common.Infrastructure.Steering
{
    public class MovementManager
    {
        private const float WANDER_ANGLE_CHANGE = 0.001f;
        private readonly Vector3 XZ2DVector = Vector3.right + Vector3.forward;

        private readonly IBoid host = default;
        private readonly float wanderCircleDistance = default;
        private readonly float wanderCircleRadius = default;

        private float wanderAngle = default;
        private Vector3 steeringForce = Vector3.zero;

        public MovementManager(IBoid host, float circleDistance = 1f, float circleRadius = 1f)
        {
            this.host = host;
            wanderCircleDistance = circleDistance;
            wanderCircleRadius = circleRadius;
        }

        public void Seek(Vector3 target, float slowingRadius = 20f)
        {
            var force = DoSeek(target, slowingRadius);
            steeringForce += force;
        }

        public void Arrive(Vector3 target, float slowingRadius = 20f)
        {
            Seek(target, slowingRadius);
        }

        public void Flee(Vector3 target)
        {
            var force = DoFlee(target);
            steeringForce += force;
        }


        public void Separation(List<IBoid> boids, float radius = 1f, float maxSeparation = 10f)
        {
            var force = DoSeparation(boids, radius , maxSeparation);
            steeringForce += force;
        }

        public void Wander()
        {
            var force = DoWander();
            steeringForce += force;
        }

        public void Evade(IBoid target, float slowingRadius = 0f)
        {
            var force = DoEvade(target, slowingRadius);
            steeringForce += force;
        }

        public void Pursuit(IBoid target)
        {
            var force = DoPursuit(target);
            steeringForce += force;
        }

        public void FollowLeader(PalinPlayer owner)
        {
            var force = DoFollowLeader(owner);
            steeringForce += force;
        }

        public void Reset()
        {
            steeringForce = Vector3.zero;
            wanderAngle = default;
        }

        public void ResetCalculatedForce()
        {
            steeringForce = Vector3.zero;
        }

        public Vector3 GetCalculatedForce()
        {
            steeringForce.Scale(XZ2DVector);
            return steeringForce;
        }

        public void CollisionAvoidance(IBoid owner, List<IBoid> obstacles, float avoidanceRadius, float maxAvoidAhead, float avoidForce)
        {
            var force = DoCollisionAvoidance(owner, obstacles, avoidanceRadius, maxAvoidAhead, avoidForce);
            steeringForce += force;
        }

        private Vector3 DoFlee(Vector3 target, float slowingRadius = 0f)
        {
            var force = -1 * DoSeek(target, slowingRadius);
            return force;
        }

        private Vector3 DoSeparation(List<IBoid> boids, float radius = 1f, float maxSeparation = 10f)
        {
            var force = Vector3.zero;
            var neighborCount = 0;

            foreach (var boid in boids)
            {
                if (boid == host)
                {
                    continue;
                }

                var distance = Vector3.Distance(host.Position, boid.Position);
                var tooFarToBeNeighbor = distance > radius;
                if (tooFarToBeNeighbor)
                {
                    continue;
                }


                force += boid.Position - host.Position;
                neighborCount++;
            }

            if (neighborCount != 0)
            {
                force /= neighborCount;
                force *= -1;
            }

            force = Normalize(force);
            force *= maxSeparation;

            return force;
        }

        private Vector3 DoSeek(Vector3 target, float slowingRadius = 0f)
        {
            var desired = target - host.Position;
            var distance = desired.magnitude;

            desired = desired.normalized;

            desired *= host.MaxSpeed;
            if (distance <= slowingRadius)
            {
                desired *= distance / slowingRadius;
            }

            var force = desired;
            return force;
        }

        private Vector3 DoPursuit(IBoid target)
        {
            var distance = target.Position - host.Position;

            var updatesNeeded = distance.magnitude / host.MaxSpeed;

            var tv = target.Velocity;
            tv *= updatesNeeded;

            var currentTargetPosition = target.Position;
            var targetFuturePosition = currentTargetPosition + tv;

            return DoSeek(targetFuturePosition);
        }

        private Vector3 DoWander()
        {
            // Calculate the circle center
            var circleCenter = host.Velocity;

            // Unity normalize behaves weird when vectors components are too small...
           // circleCenter = Normalize(circleCenter);

            circleCenter *= wanderCircleDistance;
            //
            // Calculate the displacement force
            var displacement = new Vector3(1f, 0f, 1f);
            displacement *= wanderCircleRadius;
            //
            // Randomly change the vector direction
            // by making it change its current angle
            displacement = SetAngle(displacement, wanderAngle);
            //
            // Change wanderAngle just a bit, so it
            // won't have the same value in the
            // next game frame.

            wanderAngle += (Random.value - Mathf.Sin(Time.time)) * WANDER_ANGLE_CHANGE;
            wanderAngle += WANDER_ANGLE_CHANGE;
            //
            // Finally calculate and return the wander force
            var wanderForce = circleCenter + displacement;
            return wanderForce;
        }

        private Vector3 DoEvade(IBoid target, float slowingRadius = 0f)
        {
            var distance = target.Position - host.Position;
            var updatesAhead = distance.magnitude / host.MaxSpeed;
            var futurePosition = target.Position + target.Velocity * updatesAhead;
            return DoFlee(futurePosition, slowingRadius);
        }



        private Vector3 DoFollowLeader(PalinPlayer owner)
        {
            const float LEADER_BEHIND_DIST = 2f;
            var tv = owner.Velocity.normalized * LEADER_BEHIND_DIST;
            var ahead = owner.Position + tv;

            return DoSeek(ahead, 5);
        }



        private Vector3 DoCollisionAvoidance(IBoid owner, List<IBoid> obstacles, float avoidanceRadius, float maxAvoidAhead, float avoidForce)
        {
            var tv = owner.Velocity.normalized * maxAvoidAhead * owner.Velocity.magnitude / owner.MaxSpeed;

            var ahead = owner.Position + tv;

            var mostThreateningObstacle = default(IBoid);


            foreach (var obstacle in obstacles)
            {
                if (obstacle == null)
                {
                    continue;
                }

                const float MAX_AVOID_AHEAD = 10;
                var nextAhead = owner.Position +
                                owner.Velocity.normalized *
                                (MAX_AVOID_AHEAD / 2f) *
                                owner.Velocity.magnitude / owner.MaxSpeed;

                var tooCloseToOwner = Vector3.Distance(owner.Position, obstacle.Position) <= avoidanceRadius;
                var tooCloseToAhead = Vector3.Distance(ahead, obstacle.Position) <= avoidanceRadius;
                var tooCloseToNextAhead = Vector3.Distance(nextAhead, obstacle.Position) <= avoidanceRadius;

                if (!tooCloseToOwner && !tooCloseToAhead && !tooCloseToNextAhead)
                {
                    continue;
                }

                if (mostThreateningObstacle == null || Vector3.Distance(owner.Position, obstacle.Position) < Vector3.Distance(owner.Position, mostThreateningObstacle.Position))
                {
                    mostThreateningObstacle = obstacle;
                }
            }


            if (mostThreateningObstacle == null)
            {
                return Vector3.zero;
            }

            var avoidance = ahead - mostThreateningObstacle.Position;
            return avoidance.normalized * avoidForce;
        }


        private Vector3 SetAngle(Vector3 vector, float value)
        {
            var len = vector.magnitude;
            vector.x = Mathf.Cos(value) * len;
            vector.z = Mathf.Sin(value) * len;

            return vector;
        }


        private Vector3 Normalize(Vector3 vector)
        {
            var magnitude = Vector3.Magnitude(vector);
            if (magnitude > 1E-05f)
            {
                vector /= magnitude;
            }
            else
            {
                vector = Vector3.zero;
            }

            return vector;
        }
    }
}
