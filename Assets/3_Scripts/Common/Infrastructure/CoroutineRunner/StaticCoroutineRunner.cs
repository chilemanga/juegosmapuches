using System.Collections;
using UnityEngine;

namespace _3_Scripts.Common.Infrastructure.CoroutineRunner
{
    public class StaticCoroutineRunner : MonoBehaviour
    {
        public static StaticCoroutineRunner Instance { get; private set; }

        private void Awake()
        {
            Instance = this;
        }

        public Coroutine Run(IEnumerator coroutine)
        {
            return StartCoroutine(coroutine);
        }
    }
}
