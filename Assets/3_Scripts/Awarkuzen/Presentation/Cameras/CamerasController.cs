using System.Collections;
using KurGames.Mapuches.Awarkuzen.Domain.GameFlow;
using UnityEngine;

namespace KurGames.Mapuches.Awarkuzen.Presentation.Cameras
{
    public class CamerasController : MonoBehaviour
    {
        [SerializeField] private GameObject startingView = null;
        [SerializeField] private GameObject turnResults = null;

        [SerializeField] private GameObject p1ExecutingTurn = null;
        [SerializeField] private GameObject p1ZoomInTurnResult = null;

        [SerializeField] private GameObject p2ExecutingTurn = null;
        [SerializeField] private GameObject p2ZoomInTurnResult = null;

        [SerializeField] private float waitingTimeForResults = 2f;

        private void Awake()
        {
            GameFlowService.OnGameStart += OnGameStart;
            GameFlowService.OnPlayerTurnExecuting += OnPlayerTurnExecuting;
            GameFlowService.OnPlayerTurnSetResults += OnPlayerTurnShowResults;
            GameFlowService.OnPlayerTurnFinish += OnPlayerTurnFinish;
        }

        private void OnDestroy()
        {
            GameFlowService.OnGameStart -= OnGameStart;
            GameFlowService.OnPlayerTurnExecuting -= OnPlayerTurnExecuting;
            GameFlowService.OnPlayerTurnSetResults -= OnPlayerTurnShowResults;
            GameFlowService.OnPlayerTurnFinish -= OnPlayerTurnFinish;
        }

        private void OnGameStart()
        {
            SetStartingView();
        }

        private void OnPlayerTurnExecuting(GamePlayer gamePlayer)
        {
            if (gamePlayer.IsHumanPlayer)
            {
                Player1ExecutingTurn();
            }
            else
            {
                Player2ExecutingTurn();
            }
        }

        private void OnPlayerTurnShowResults(GamePlayer gamePlayer)
        {
            TurnZoomInResults(gamePlayer);
        }

        private void OnPlayerTurnFinish(GamePlayer gamePlayer)
        {
            TurnResults();
        }


        private void SetStartingView()
        {
            startingView.SetActive(true);
            turnResults.SetActive(false);

            p1ExecutingTurn.SetActive(false);
            p2ExecutingTurn.SetActive(false);
        }

        private void Player1ExecutingTurn()
        {
            turnResults.SetActive(false);

            p1ExecutingTurn.SetActive(true);
        }

        private void TurnZoomInResults(GamePlayer gamePlayer)
        {
            p1ExecutingTurn.SetActive(false);

            p2ExecutingTurn.SetActive(false);

            p1ZoomInTurnResult.SetActive(gamePlayer.IsHumanPlayer);
            p2ZoomInTurnResult.SetActive(!gamePlayer.IsHumanPlayer);

            turnResults.SetActive(false);

            StartCoroutine(WaitForTurnResults(gamePlayer));
        }

        private IEnumerator WaitForTurnResults(GamePlayer gamePlayer)
        {
            yield return new WaitForSeconds(waitingTimeForResults);
            // TODO: Perhaps, calling a callback should be better instead of handling the game flow here
            GameFlowService.FinishTurnUIDone(gamePlayer);
        }

        private void TurnResults()
        {
            p1ExecutingTurn.SetActive(false);

            p2ExecutingTurn.SetActive(false);

            p1ZoomInTurnResult.SetActive(false);
            p2ZoomInTurnResult.SetActive(false);

            turnResults.SetActive(true);
        }

        private void Player2ExecutingTurn()
        {
            turnResults.SetActive(false);

            p2ExecutingTurn.SetActive(true);
        }
    }
}
