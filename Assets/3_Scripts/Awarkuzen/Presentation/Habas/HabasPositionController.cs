using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace KurGames.Mapuches.Awarkuzen.Presentation.Habas
{
    public class HabasPositionController : MonoBehaviour
    {
        private List<HabaController> habaControllers = null;

        private void Awake() 
        {
            habaControllers = transform.GetComponentsInChildren<HabaController>().ToList();
        }

        public void Restart()
        {
            habaControllers.ForEach(c => c.RestartToInitialPosition());
        }

        public bool AreAllOnTheGround()
        {
            var notTouching = habaControllers.Count(habaController => !habaController.IsTouchingGround());

            return notTouching == 0;
        }

        public void AllowMovement()
        {
            habaControllers.ForEach(c => c.AllowMovement());
        }
    }
}
