using System;
using UnityEngine;
using UnityEngine.Serialization;

namespace KurGames.Mapuches.Awarkuzen.Presentation.Habas
{
    public class HabaController : MonoBehaviour
    {
        [SerializeField] private Transform upSideTransform = null;
        [SerializeField] private Transform downSideTransform = null;
        [SerializeField] private GameObject haba3dModel = null;
        [SerializeField] private Rigidbody habaRigidbody = null;
        [SerializeField] private Outline outline;

        public void RestartToInitialPosition()
        {
            habaRigidbody.constraints = RigidbodyConstraints.FreezeAll;
            habaRigidbody.velocity = Vector3.zero;

            haba3dModel.transform.localPosition = Vector3.zero;

            SetOutlineColor(Color.white);
        }

        public void SetOutlineColor(Color outlineColor)
        {
            if (outline != null)
            {
                outline.OutlineColor = outlineColor;
            }
        }

        public void SetOutlineColorByOrientation()
        {
            SetOutlineColor(IsUpside() ? Color.cyan : Color.magenta);
        }

        public void ResetOutlineColor()
        {
            SetOutlineColor(Color.white);
        }

        public bool IsUpside()
        {
            return upSideTransform.position.y >= downSideTransform.position.y;
        }

        public bool IsTouchingGround()
        {
            return haba3dModel.transform.position.y < 2f;
        }

        public void AllowMovement()
        {
            habaRigidbody.constraints = RigidbodyConstraints.None;
        }
    }
}
