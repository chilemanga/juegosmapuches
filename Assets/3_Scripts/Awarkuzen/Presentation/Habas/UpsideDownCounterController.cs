using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace KurGames.Mapuches.Awarkuzen.Presentation.Habas
{
    public class UpsideDownCounterController : MonoBehaviour
    {
        private List<HabaController> habaControllers = null;

        public List<HabaController> GetAllHabas()
        {
            return habaControllers;
        }

        private void Awake()
        {
            habaControllers = transform.GetComponentsInChildren<HabaController>().ToList();
        }

        public int TotalHabasUp()
        {
            return habaControllers.Count(h => h.IsUpside());
        }

        public int TotalHabas()
        {
            return habaControllers.Count();
        }
    }
}
