﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace KurGames.Mapuches.Awarkuzen.Presentation.Utils
{
    public class PulsingEffect : MonoBehaviour
    {
        [SerializeField]
        private float loopsPerSecs = 2f;

        [SerializeField]
        private float scaleFactor = 0.2f;

        [SerializeField]
        private bool disabled = default;

        private Vector3 originalScale = Vector3.one;

        private void Awake()
        {
            originalScale = transform.localScale;
        }

        private void OnEnable()
        {
            StopAllCoroutines();
            StartCoroutine(PulseMovement());
        }

        private void OnDisable()
        {
            StopAllCoroutines();
            transform.localScale = originalScale;
        }

        private IEnumerator PulseMovement()
        {
            var cachedTransform = transform;
            var lerpStep = 0f;
            var goingUp = true;
            var maxScale = originalScale * (1f + scaleFactor);

            while (!disabled)
            {
                lerpStep = Mathf.Clamp01(lerpStep + (goingUp ? 1 : -1 ) * loopsPerSecs * Time.deltaTime);
                cachedTransform.localScale = Vector3.Lerp(originalScale, maxScale, lerpStep);

                var currentScale = cachedTransform.localScale.x;
#if VERBOSE
                Debug.Log($" [{originalScale.x}- {currentScale}-{maxScale.x}] loopsPerSecs:[{loopsPerSecs}] lerpStep:[{lerpStep}], goingUp?: [{goingUp}]");
#endif
                yield return new WaitForEndOfFrame();

                if (currentScale <= originalScale.x || maxScale.x <= currentScale)
                {
                    goingUp = !goingUp;
                }
            }

            yield return null;
        }

        public void Configure(float newLoopsPerSecs, float newScaleFactor)
        {
            StopAllCoroutines();
            loopsPerSecs = newLoopsPerSecs;
            scaleFactor = newScaleFactor;
        }
    }
}
