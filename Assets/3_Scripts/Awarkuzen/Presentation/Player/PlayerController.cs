using System.Collections.Generic;
using System.Collections;
using KurGames.Mapuches.Awarkuzen.Domain.GameFlow;
using KurGames.Mapuches.Awarkuzen.Domain.GameRules;
using KurGames.Mapuches.Awarkuzen.Presentation.Habas;
using UnityEngine;
using KurGames.Mapuches.Awarkuzen.Presentation.Input;

namespace KurGames.Mapuches.Awarkuzen.Presentation.Player
{
    public class PlayerController : MonoBehaviour
    {
        private const float TURN_TIME_LIMIT = 10f;

        [SerializeField] private UpsideDownCounterController upsideDownCounterController = null;
        [SerializeField] private HabasPositionController habasPositionController = null;
        [SerializeField] private HandController handController = null;
        [SerializeField] private AccelerometerController accelerometerController = default;

        private GamePlayer gamePlayer = null;

        private void Awake()
        {
            GameFlowService.OnGameStart += OnGameStart;
            GameFlowService.OnPlayerTurnStart += OnPlayerTurnStart;
            GameFlowService.OnPlayerTurnFinish += OnPlayerTurnFinish;
        }

        private void OnDestroy()
        {
            GameFlowService.OnGameStart -= OnGameStart;
            GameFlowService.OnPlayerTurnStart -= OnPlayerTurnStart;
            GameFlowService.OnPlayerTurnFinish -= OnPlayerTurnFinish;
        }

        public void SetPlayer(GamePlayer player)
        {
            gamePlayer = player;
        }

        private void OnGameStart()
        {
            handController.Restart();
            if (accelerometerController != null)
            {
                accelerometerController.IsMovementAllowed = GameRulesService.IsInHardMode;
            }
        }

        private void OnPlayerTurnStart(GamePlayer currentGamePlayer)
        {
            if (currentGamePlayer != gamePlayer)
            {
                return;
            }
            if (accelerometerController != null)
            {
                accelerometerController.IsMovementAllowed = GameRulesService.IsInHardMode;
            }
            habasPositionController.AllowMovement();
            handController.AllowMovement(gamePlayer.IsHumanPlayer);
            GameFlowService.ExecutingTurn(gamePlayer);

            StartCoroutine(CheckForTurnFinished());
        }

        private IEnumerator CheckForTurnFinished()
        {
            var turnTimeLeft = TURN_TIME_LIMIT;

            while (!habasPositionController.AreAllOnTheGround() && turnTimeLeft > 0f)
            {
                turnTimeLeft -= Time.deltaTime;
                yield return null;
            }

            yield return new WaitForSeconds(.2f);

            TintMyHabas();

            yield return new WaitForSeconds(.65f);

            CalculatePlayerScore();
        }

        private void TintMyHabas()
        {
            foreach (var haba in upsideDownCounterController.GetAllHabas())
            {
                haba.SetOutlineColorByOrientation();
            }
        }
        private void ResetMyHabasTint()
        {
            foreach (var haba in upsideDownCounterController.GetAllHabas())
            {
                haba.ResetOutlineColor();
            }
        }

        private void CalculatePlayerScore()
        {
            var totalUp = upsideDownCounterController.TotalHabasUp();
            var total = upsideDownCounterController.TotalHabas();

            var totalScore = GameRulesService.GetScore(totalUp, total);
            var addExtraTurn = GameRulesService.PlayerEarnedANewTurn(totalUp, total);

            Debug.Log($"up/Total: [{totalUp}/{total}], <color=green>score -> {totalScore}</color>, addExtraTurn: [<color=orange>{addExtraTurn}</color>]");

            GameFlowService.SetResultsOfTurn(gamePlayer, totalScore, addExtraTurn);
        }

        private void OnPlayerTurnFinish(GamePlayer currentGamePlayer)
        {
            if (currentGamePlayer != gamePlayer)
            {
                return;
            }
            if (accelerometerController != null)
            {
                accelerometerController.IsMovementAllowed = false;
            }

            habasPositionController.Restart();
            handController.Restart();
            ResetMyHabasTint();
        }

        public GamePlayer GetPlayer()
        {
            return gamePlayer;
        }
    }
}
