using System;
using System.Collections;
using UnityEngine;

namespace KurGames.Mapuches.Awarkuzen.Presentation.Player
{
    public class HandController : MonoBehaviour
    {
        [SerializeField] private Animation handAnimation = null;
        [SerializeField] private string handIdleAnimationName = "Hand_Idle";
        [SerializeField] private string handThrowHabasAnimationName = "Hand_ThrowHabas";

        private bool canMove = false;
        private Vector3 initialPosition = Vector3.zero;
        private Quaternion initialRotation = Quaternion.identity;

        private const float THROW_AGAIN_WAITING_TIME = 5F;
        private bool blockedByTime = false;

        private Coroutine idleCoroutine = null;

        private void Awake()
        {
            initialPosition = transform.localPosition;
            initialRotation = transform.rotation;
        }

        public void AllowMovement(bool humanMovement)
        {
            Restart();
            idleCoroutine = StartCoroutine(StartIdleMovement());

            if (humanMovement)
            {
                canMove = true;
            }
            else
            {
                StartCoroutine(EmulateMovement());
            }
        }

        public void Restart()
        {
            canMove = false;

            transform.localPosition = initialPosition;
            transform.rotation = initialRotation;

            if (idleCoroutine != null)
            {
                StopCoroutine(idleCoroutine);
            }
        }

        public void TryToThrowHabas()
        {
            Debug.Log($"Trying to throw habas from player. Can move? -> {canMove}, blocked by time? {blockedByTime}.");
            if (!canMove || blockedByTime)
            {
                return;
            }
            blockedByTime = true;
            ThrowHabas();
            Invoke("UnlockThrowByTime", THROW_AGAIN_WAITING_TIME);
        }

        private void UnlockThrowByTime()
        {
            blockedByTime = false;
        }

        private void ThrowHabas()
        {
            if (idleCoroutine != null)
            {
                StopCoroutine(idleCoroutine);
            }

            handAnimation.Play(handThrowHabasAnimationName);
        }

        private IEnumerator StartIdleMovement()
        {
            while (true)
            {
                handAnimation.Play(handIdleAnimationName);

                while (handAnimation.isPlaying)
                {
                    yield return null;
                }
            }
        }

        private IEnumerator EmulateMovement()
        {
            yield return new WaitForSeconds(3f);

            ThrowHabas();
        }

        private void Update()
        {
            if (!canMove)
            {
                return;
            }

            if (UnityEngine.Input.GetKey(KeyCode.Space))
            {
                ThrowHabas();
            }
        }

    }
}
