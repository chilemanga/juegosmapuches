using KurGames.Mapuches.Awarkuzen.Domain.GameFlow;
using UnityEngine;
using TMPro;

namespace KurGames.Mapuches.Awarkuzen.Presentation.Player
{
    public class ScoreController : MonoBehaviour
    {
        [SerializeField] private bool isPlayer1 = default;
        [SerializeField] private TextMeshProUGUI rockScoreValue = default;
        [SerializeField] private TextMeshProUGUI stickScoreValue = default;
        [SerializeField] private TextMeshProUGUI totalScoreValue = default;

        private const float kStickValue = 5f;
        private const float kRockValue = 1f;

        private void Awake()
        {
            GameFlowService.OnGameStart += OnGameStart;
            GameFlowService.OnPlayerTurnSetResults += UpdateScore;
        }

        private void OnDestroy()
        {
            GameFlowService.OnGameStart -= OnGameStart;
            GameFlowService.OnPlayerTurnSetResults -= UpdateScore;
        }

        private void UpdateScore(GamePlayer gamePlayer)
        {
            if ((gamePlayer.IsHumanPlayer && isPlayer1)
                || (!gamePlayer.IsHumanPlayer && !isPlayer1))
            {
                UpdateScore(gamePlayer.CurrentScore);
            }
        }

        private void OnGameStart()
        {
            UpdateScore((int)default);
        }

        private void UpdateScore(int scoreAmount)
        {
            totalScoreValue.text = scoreAmount.ToString();
            stickScoreValue.text = Mathf.FloorToInt((float)scoreAmount / (float)kStickValue).ToString();
            rockScoreValue.text = Mathf.FloorToInt(((float)scoreAmount % (float)kStickValue) * kRockValue).ToString();
        }
    }
}
