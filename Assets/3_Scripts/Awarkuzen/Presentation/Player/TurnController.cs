using KurGames.Mapuches.Awarkuzen.Domain.GameFlow;
using UnityEngine;

namespace KurGames.Mapuches.Awarkuzen.Presentation.Player
{
    public class TurnController : MonoBehaviour
    {
        [SerializeField] private bool isHumanPlayerSetting = default;
        [SerializeField] private GameObject turnObject = default;

        private void Awake()
        {
            GameFlowService.OnPlayerTurnStart += OnPlayerTurnStart;
            GameFlowService.OnPlayerTurnFinish += OnPlayerTurnFinish;
        }

        private void OnDestroy()
        {
            GameFlowService.OnPlayerTurnStart -= OnPlayerTurnStart;
            GameFlowService.OnPlayerTurnFinish -= OnPlayerTurnFinish;
        }

        private void OnPlayerTurnStart(GamePlayer currentGamePlayer)
        {
            var areCurrentPlayerSettings = currentGamePlayer.IsHumanPlayer == isHumanPlayerSetting;
            turnObject.SetActive(areCurrentPlayerSettings);
        }

        private void OnPlayerTurnFinish(GamePlayer currentGamePlayer)
        {
            var areCurrentPlayerSettings = currentGamePlayer.IsHumanPlayer == isHumanPlayerSetting;
            turnObject.SetActive(areCurrentPlayerSettings);
        }
    }
}
