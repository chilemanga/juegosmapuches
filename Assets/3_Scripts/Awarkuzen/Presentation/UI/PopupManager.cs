﻿using KurGames.Mapuches.Awarkuzen.Domain.GameFlow;
using KurGames.Mapuches.Common.Presentation.UI;
using UnityEngine;

namespace KurGames.Mapuches.Awarkuzen.Presentation.UI
{
    public class PopupManager : BasePopupManager
    {
        protected override void Awake()
        {
            base.Awake();
            GameFlowService.OnGameFinish += OnGameFinish;
        }

        private void OnGameFinish(GamePlayer gamePlayer)
        {
            base.OnGameFinish(gamePlayer.IsHumanPlayer);
        }

#if UNITY_EDITOR
        private void Update()
        {
            if (UnityEngine.Input.GetKeyDown(KeyCode.W))
            {
                //OnGameFinish(new GamePlayer(true));
                GameFlowService.FinishGame(new GamePlayer(true));
            }
            else if (UnityEngine.Input.GetKeyDown(KeyCode.L))
            {
                //OnGameFinish(new GamePlayer(false));
                GameFlowService.FinishGame(new GamePlayer(false));
            }
        }
#endif
    }
}