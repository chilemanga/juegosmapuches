﻿using UnityEngine;

namespace KurGames.Mapuches.Awarkuzen.Domain.GameRules
{
    public class DifficultySelectorController : MonoBehaviour
    {
        public void SetRequiredScoreToWin(int requiredScore)
        {
            GameRulesService.SetScoreToWin(requiredScore);
        }
    }
}
