using UnityEngine;

namespace KurGames.Mapuches.Awarkuzen.Presentation.Input
{
    [RequireComponent(typeof(Rigidbody))]
    public class AccelerometerController : MonoBehaviour
    {
        [Range(0.2f, 2f)]
        [SerializeField] private float moveSpeedModifier = 0.5f;

        private Animator animator;
        private Rigidbody rb;
        private bool moveAllowed;
        public bool IsMovementAllowed
        {
            get
            {
                return moveAllowed;
            }
            set
            {
                moveAllowed = value;
            }
        }
        private float dirX, dirY, dirZ;

        private void Awake()
        {
            moveAllowed = false;
            rb = GetComponent<Rigidbody>();
            animator = GetComponent<Animator>();
        }

        private void Update()
        {
            if (!moveAllowed)
            {
                return;
            }
            // Getting devices accelerometer data in X and Y direction
            // multiplied by move speed modifier
            dirX = UnityEngine.Input.acceleration.x * moveSpeedModifier;
            dirY = UnityEngine.Input.acceleration.y * moveSpeedModifier;
            dirZ = UnityEngine.Input.acceleration.z * moveSpeedModifier;
        }

        private void FixedUpdate()
        {
            if (!moveAllowed)
            {
                return;
            }
            // Setting a velocity to Rigidbody component according to accelerometer data
            rb.velocity = new Vector3(rb.velocity.x + dirX, rb.velocity.y + dirY, rb.velocity.z + dirZ);

            Debug.DrawRay(transform.position, new Vector3(dirX, dirY, dirZ), Color.cyan);
        }
    }
}
