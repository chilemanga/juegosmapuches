using System;
using System.Collections;
using KurGames.Mapuches.Awarkuzen.Domain.GameFlow;
using KurGames.Mapuches.Awarkuzen.Domain.GameRules;
using KurGames.Mapuches.Awarkuzen.Presentation.Player;
using KurGames.Mapuches.Common.Presentation.GameFlow;
using UnityEngine;

namespace KurGames.Mapuches.Awarkuzen.Presentation.GameFlow
{
    public class GameFlowController : BaseGameFlowController
    {
        [SerializeField] private PlayerController player1Controllers = null;
        [SerializeField] private PlayerController player2Controllers = null;

        protected override void Awake()
        {
            base.Awake();

            GameFlowService.OnPlayerTurnFinish += OnFinishTurn;
            GameFlowService.OnGameFinish += OnGameFinish;
        }

        private void OnDestroy()
        {
            GameFlowService.OnPlayerTurnFinish -= OnFinishTurn;
            GameFlowService.OnGameFinish -= OnGameFinish;
        }

        private void Start()
        {
            var gamePlayerHuman = new GamePlayer(isHumanPlayer: true);
            player1Controllers.SetPlayer(gamePlayerHuman);

            var gamePlayerIA = new GamePlayer(isHumanPlayer: false);
            player2Controllers.SetPlayer(gamePlayerIA);
        }

        private void OnFinishTurn(GamePlayer gamePlayer)
        {
        }

        private void OnGameFinish(GamePlayer gamePlayer)
        {
            // TODO: This is now managed by win or lose popups
            //StartCoroutine(RestartCR());
        }

        private IEnumerator RestartCR()
        {
            yield return new WaitForSeconds(1f);

            StartGame();

        }

        public override void StartGame(int id = 1)
        {
            var p1 = player1Controllers.GetPlayer();
            var p2 = player2Controllers.GetPlayer();
            GameFlowService.SetPlayers(new[] { p1, p2 });

            GameFlowService.StartGame(GameRulesService.RequiredScoreToWin);
        }
    }
}
