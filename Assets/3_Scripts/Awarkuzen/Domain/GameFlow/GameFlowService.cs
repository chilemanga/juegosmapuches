using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace KurGames.Mapuches.Awarkuzen.Domain.GameFlow
{
    // TODO; use zenject instead of an static class...
    public static class GameFlowService
    {
        private static List<GamePlayer> players = new List<GamePlayer>();
        private static GamePlayer currentPlayer = null;
        public static event Action OnGameStart = delegate { };
        public static event Action<GamePlayer> OnPlayerTurnStart = delegate { };
        public static event Action<GamePlayer> OnPlayerTurnExecuting = delegate { };
        public static event Action<GamePlayer> OnPlayerTurnSetResults = delegate { };
        public static event Action<GamePlayer> OnPlayerTurnFinish = delegate { };
        public static event Action<GamePlayer> OnGameFinish = delegate {};

        private static int requiredScoreToWin = default;

        public static void SetPlayers(IEnumerable<GamePlayer> newPlayers)
        {
            Debug.Log("<color=orange>GameFlowService: SetPlayers</color>");
            players.Clear();
            currentPlayer = null;

            foreach (var player in newPlayers)
            {
                Debug.Log($"GameFlowService: Add Player: [<color=cyan>isHuman:{player.IsHumanPlayer}</color>]");
                players.Add(player);
            }
        }

        public static void StartGame(int scoreToWin)
        {
            Debug.Log($"<color=orange>GameFlowService: StartGame, scoreToWin; [{scoreToWin}]</color>");
            requiredScoreToWin = scoreToWin;

            foreach (var gamePlayer in players)
            {
                gamePlayer.Restart();
            }

            OnGameStart?.Invoke();

            StartNextTurn();
        }

        private static void StartNextTurn()
        {
            Debug.Log("<color=green>GameFlowService: StartNextTurn</color>");

            if (currentPlayer == null)
            {
                currentPlayer = players.First();
                OnPlayerTurnStart?.Invoke(currentPlayer);
                return;
            }

            if (currentPlayer.ExtraTurn)
            {
                OnPlayerTurnStart?.Invoke(currentPlayer);
                return;
            }

            var currentPlayerIndex = players.FindIndex(p => p == currentPlayer);
            currentPlayerIndex++;

            if (currentPlayerIndex >= players.Count)
            {
                currentPlayerIndex = 0;
            }

            currentPlayer = players[currentPlayerIndex];

            OnPlayerTurnStart?.Invoke(currentPlayer);
        }

        public static void ExecutingTurn(GamePlayer currentPlayer)
        {
            Debug.Log($"GameFlowService: ExecutingTurn [<color=cyan>isHuman:{currentPlayer.IsHumanPlayer}</color>]");
            OnPlayerTurnExecuting?.Invoke(currentPlayer);
        }

        public static void SetResultsOfTurn(GamePlayer currentPlayer, int turnScore, bool addExtraTurn)
        {
            Debug.Log("GameFlowService: SetResultsOfTurn");

            currentPlayer.AddScore(turnScore, addExtraTurn);

            OnPlayerTurnSetResults?.Invoke(currentPlayer);
        }

        public static void FinishTurnUIDone(GamePlayer currentPlayer)
        {
            Debug.Log("<color=yellow>GameFlowService: FinishTurn</color>");

            OnPlayerTurnFinish?.Invoke(currentPlayer);

            if (currentPlayer.CurrentScore >= requiredScoreToWin)
            {
                Debug.Log($"player (<color=cyan>isHuman:[{currentPlayer.IsHumanPlayer}]</color>) won with [{currentPlayer.CurrentScore}] points");
                FinishGame(currentPlayer);
                return;
            }

            StartNextTurn();
        }

        public static void FinishGame(GamePlayer winner)
        {
            Debug.Log($"<color=yellow>GameFlowService: FinishGame winner: [<color=cyan>isHuman:{winner.IsHumanPlayer}</color>], score:[{winner.CurrentScore}]</color>");
            OnGameFinish?.Invoke(winner);
        }


    }
}
