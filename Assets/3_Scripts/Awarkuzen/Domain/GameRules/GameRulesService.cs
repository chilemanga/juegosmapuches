using UnityEngine;

namespace KurGames.Mapuches.Awarkuzen.Domain.GameRules
{
    // TODO; use zenject instead of an static class...
    public static class GameRulesService
    {

        public static readonly int scoreByAllTheSame = 2;
        public static readonly int scoreByHalfTurned = 1;
        public static int RequiredScoreToWin { get; private set; } = 10;
        public static bool IsInHardMode { get { return RequiredScoreToWin > 10; } }

        public static void SetScoreToWin(int newRequiredScoreToWin)
        {
            Debug.Log($"Required score to win was <color=green>{RequiredScoreToWin}</color>, but now it will be <color=yellow>{newRequiredScoreToWin}</color>.");
            RequiredScoreToWin = newRequiredScoreToWin;
        }

        public static bool PlayerEarnedANewTurn(int turnedUp, int total)
        {
            return GetScore(turnedUp, total) > 0;
        }

        public static int GetScore(int turnedUp, int total)
        {
            Debug.Log($"GetScore: turnedUp {turnedUp}, total {total}...");
            // All turned up or all turned down
            if (turnedUp == 0 || turnedUp == total)
            {
                return scoreByAllTheSame;
            }

            int halfTotal = (int)((float)total * .5);
            // Half turned up
            if (turnedUp == halfTotal)
            {
                Debug.Log($"GetScore: turnedUp {turnedUp}, half {halfTotal}: score {turnedUp == halfTotal} -> 1");
                return scoreByHalfTurned;
            }

            Debug.Log($"GetScore: turnedUp {turnedUp}, half {halfTotal}: score {turnedUp == halfTotal} -> 0");
            return 0;
        }
    }
}
