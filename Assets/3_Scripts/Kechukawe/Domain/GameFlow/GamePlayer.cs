using UnityEngine;

namespace KurGames.Mapuches.Kechukawe.Domain.GameFlow
{
    public class GamePlayer
    {
        public int CurrentScore { get; private set; }
        public bool IsHumanPlayer { get; private set; }

        public GamePlayer(bool isHumanPlayer)
        {
            IsHumanPlayer = isHumanPlayer;
            Restart();
        }

        public void AddScore(int score)
        {
            CurrentScore += score;
            Debug.Log($"GamePlayer [<color=cyan>isHuman:{IsHumanPlayer}</color>] AddScore: [<color=green>{score}</color>], updated CurrentScore:[<color=green>{CurrentScore}</color>]");
        }

        public void Restart()
        {
            CurrentScore = 0;
        }
    }
}
