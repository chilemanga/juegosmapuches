using System.Collections;
using KurGames.Mapuches.Kechukawe.Domain.GameFlow;
using UnityEngine;

namespace KurGames.Mapuches.Kechukawe.Presentation.Cameras
{
    public class CamerasController : MonoBehaviour
    {
        [SerializeField] private GameObject startingView = null;

        [SerializeField] private float waitingTimeForResults = 2f;

        private void Awake()
        {
            GameFlowService.OnGameStart += OnGameStart;
            GameFlowService.OnPlayerTurnExecuting += OnPlayerTurnExecuting;
            GameFlowService.OnPlayerTurnSetResults += OnPlayerTurnShowResults;
            GameFlowService.OnPlayerTurnFinish += OnPlayerTurnFinish;
        }

        private void OnDestroy()
        {
            GameFlowService.OnGameStart -= OnGameStart;
            GameFlowService.OnPlayerTurnExecuting -= OnPlayerTurnExecuting;
            GameFlowService.OnPlayerTurnSetResults -= OnPlayerTurnShowResults;
            GameFlowService.OnPlayerTurnFinish -= OnPlayerTurnFinish;
        }

        private void OnGameStart()
        {
            SetStartingView();
        }

        private void OnPlayerTurnExecuting(GamePlayer gamePlayer)
        {
            if (gamePlayer.IsHumanPlayer)
            {
                Player1ExecutingTurn();
            }
            else
            {
                Player2ExecutingTurn();
            }
        }

        private void OnPlayerTurnShowResults(GamePlayer gamePlayer)
        {
            TurnZoomInResults(gamePlayer);
        }

        private void OnPlayerTurnFinish(GamePlayer gamePlayer)
        {
            TurnResults();
        }


        private void SetStartingView()
        {
            startingView.SetActive(true);
        }

        private void Player1ExecutingTurn()
        {
        }

        private void TurnZoomInResults(GamePlayer gamePlayer)
        {
            StartCoroutine(WaitForTurnResults(gamePlayer));
        }

        private IEnumerator WaitForTurnResults(GamePlayer gamePlayer)
        {
            yield return new WaitForSeconds(waitingTimeForResults);
            // TODO: Perhaps, calling a callback should be better instead of handling the game flow here
            GameFlowService.FinishTurnUIDone(gamePlayer);
        }

        private void TurnResults()
        {
        }

        private void Player2ExecutingTurn()
        {
        }
    }
}
