using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using KurGames.Mapuches.Kechukawe.Domain.GameFlow;

namespace KurGames.Mapuches.Kechukawe.Presentation.Player
{
    public class PlayerController : MonoBehaviour
    {
        [SerializeField] private List<GameObject> tokens = default;
        [SerializeField] private DiceController diceController = default;
        [SerializeField] private TurnController turnController = default;
        [SerializeField] private ScoreController scoreController = default;
        [SerializeField] private PlayerController otherPlayer = default;

        private GamePlayer gamePlayer = null;
        private const float ADDING_SCORE_TIME = 0.15f;
        private const int EACH_AREA_TOKENS = 12;

        //[HideInInspector]
        public List<GameObject> OtherPlayerOutsideTokens = new List<GameObject>();

        private void Awake()
        {
            GameFlowService.OnGameStart += OnGameStart;
            GameFlowService.OnPlayerTurnStart += OnPlayerTurnStart;
            GameFlowService.OnPlayerTurnFinish += OnPlayerTurnFinish;
        }

        private void OnDestroy()
        {
            GameFlowService.OnGameStart -= OnGameStart;
            GameFlowService.OnPlayerTurnStart -= OnPlayerTurnStart;
            GameFlowService.OnPlayerTurnFinish -= OnPlayerTurnFinish;
        }

        public void SetPlayer(GamePlayer player)
        {
            gamePlayer = player;
        }

        private void OnGameStart()
        {
            diceController.Reset();
            OtherPlayerOutsideTokens.Clear();
            ShowAllTokens(false);
        }

        private void ShowAllTokens(bool show)
        {
            foreach (var token in tokens)
            {
                token.SetActive(show);
            }
        }

        public GameObject ShowToken(int tokenIndex, bool show)
        {
            if (tokenIndex >= GameFlowService.GetScoreToWin() - 1)
            {
                return null;
            }
            tokens[tokenIndex].SetActive(show);
            return tokens[tokenIndex];
        }

        public void UpdateScoreDisplay(int newScore)
        {
            if (scoreController == null) return;
            scoreController.UpdateScore(newScore);
        }

        public void ReduceOneOpponentsScore(bool updateTokenDisplay = true)
        {
            int otherPlayerScore = otherPlayer.GetPlayer().CurrentScore;
            if (otherPlayerScore <= 0) return;

            if (updateTokenDisplay)
            {
                otherPlayer.ShowToken(otherPlayerScore - 1, false);
            }

            otherPlayer.GetPlayer().AddScore(-1);
            otherPlayer.UpdateScoreDisplay(otherPlayer.GetPlayer().CurrentScore);
        }

        public void ThrowDice()
        {
            turnController.EnableButton(false);
            diceController.ThrowDiceAnimated(UpdatePlayerScore);
            // HACK: Just to show how you lose:
            //diceController.ThrowDiceAnimated(UpdatePlayerScore, !gamePlayer.IsHumanPlayer ? 5 : -1);
        }

        private IEnumerator AutoThrowDice(float delay = 1f)
        {
            yield return new WaitForSeconds(delay);
            ThrowDice();
        }

        private void OnPlayerTurnStart(GamePlayer currentGamePlayer)
        {
            if (currentGamePlayer != gamePlayer)
            {
                return;
            }
            GameFlowService.ExecutingTurn(gamePlayer);

            turnController.EnableButton(true);

            if (!gamePlayer.IsHumanPlayer)
            {
                StartCoroutine(AutoThrowDice());
            }
        }

        private void UpdatePlayerScore(int scoreToAdd)
        {
            if (scoreToAdd == GameFlowService.ImmediateDiceWin)
            {
                GameFlowService.SetResultsOfTurn(gamePlayer, scoreToAdd);
                return;
            }
            StartCoroutine(ShowNewTurnResults(scoreToAdd));
        }

        private IEnumerator ShowNewTurnResults(int scoreToAdd)
        {
            int indexFrom = gamePlayer.CurrentScore;
            for (int i = 0; i < scoreToAdd; i++)
            {
                int tokenIndex = indexFrom + i;
                if (tokenIndex >= GameFlowService.GetScoreToWin() - 1)
                {
                    // Won the game!
                    break;
                }
                var shownToken = ShowToken(tokenIndex, true);

                // When tokens are taking other player's tokens, we need to reduce their score
                if (tokenIndex >= EACH_AREA_TOKENS)
                {
                    ReduceOneOpponentsScore();
                    var tokenRobbed = shownToken;
                    OtherPlayerOutsideTokens.Add(tokenRobbed);
                }

                // Should we recover tokens from other's player?

                // If tokens from the other player "outside" tokens + the ones added now
                // are more than 12, then "grab" (turn off)
                // tokens from outside to place it in my field
                if (otherPlayer.OtherPlayerOutsideTokens.Count > 0
                    && otherPlayer.OtherPlayerOutsideTokens.Count + tokenIndex >= EACH_AREA_TOKENS)
                {
                    // "Grab" the outside token and hide it...
                    int outsideIndex = otherPlayer.OtherPlayerOutsideTokens.Count - 1;
                    otherPlayer.OtherPlayerOutsideTokens[outsideIndex].SetActive(false);
                    otherPlayer.OtherPlayerOutsideTokens.RemoveAt(outsideIndex);
                    // Update score from opponent, but not its tokens display
                    ReduceOneOpponentsScore(updateTokenDisplay: false);
                }


                yield return new WaitForSeconds(ADDING_SCORE_TIME);
            }
            GameFlowService.SetResultsOfTurn(gamePlayer, scoreToAdd);
        }

        private void OnPlayerTurnFinish(GamePlayer currentGamePlayer)
        {
            if (currentGamePlayer != gamePlayer)
            {
                return;
            }
        }

        public GamePlayer GetPlayer()
        {
            return gamePlayer;
        }
    }
}
