using KurGames.Mapuches.Kechukawe.Domain.GameFlow;
using UnityEngine;

namespace KurGames.Mapuches.Kechukawe.Presentation.Player
{
    public class TurnController : MonoBehaviour
    {
        [SerializeField] private bool isHumanPlayerSetting = default;
        [SerializeField] private GameObject turnObject = default;
        [SerializeField] private UnityEngine.UI.Button button = default;

        private void Awake()
        {
            GameFlowService.OnPlayerTurnStart += OnPlayerTurnStart;
            GameFlowService.OnPlayerTurnFinish += OnPlayerTurnFinish;
        }

        private void OnDestroy()
        {
            GameFlowService.OnPlayerTurnStart -= OnPlayerTurnStart;
            GameFlowService.OnPlayerTurnFinish -= OnPlayerTurnFinish;
        }

        private void OnPlayerTurnStart(GamePlayer currentGamePlayer)
        {
            if (turnObject == null)
            {
                return;
            }
            var areCurrentPlayerSettings = currentGamePlayer.IsHumanPlayer == isHumanPlayerSetting;
            turnObject.SetActive(areCurrentPlayerSettings);
        }

        private void OnPlayerTurnFinish(GamePlayer currentGamePlayer)
        {
            if (turnObject == null)
            {
                return;
            }
            var areCurrentPlayerSettings = currentGamePlayer.IsHumanPlayer == isHumanPlayerSetting;
            turnObject.SetActive(areCurrentPlayerSettings);
        }

        public void EnableButton(bool enable)
        {
            if (button == null) return;

            button.interactable = enable;
        }
    }
}
