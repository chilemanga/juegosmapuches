using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using System;

namespace KurGames.Mapuches.Kechukawe.Presentation.Player
{
    [RequireComponent(typeof(Animator))]
    public class DiceController : MonoBehaviour
    {
        [SerializeField] private GameObject diceDisplayed = default;
        [SerializeField] private List<GameObject> diceValues = default;
        [SerializeField] private float animationDuration = 0.75f;
        [Tooltip("A value between 0 and 1 to determine the probability of getting the winning value (5)"), Range(0, 1)]
        [SerializeField] private float winningDiceValuePercentage = .01f;

        private const int MAX_VALUE = 5;
        private WaitForSeconds diceAnimationFrameWaitTime = new WaitForSeconds(0.35f);

        public bool IsDiceRolling = false;
        private List<int> probabilitiesList = new List<int>();
        private Animator diceAnimator = null;

        private void Awake()
        {
            InitializeProbabilities();
            diceAnimator = GetComponent<Animator>();
        }

        private void InitializeProbabilities()
        {
            const int totalEntries = 100;
            int amountOfValuesTotal = totalEntries;
            int amountOfWinningValues = 0;
            // Is the winning percentage accountable? (0.01 * 100 = 1; the minimum value)
            if (winningDiceValuePercentage >= 0.01f)
            {
                amountOfWinningValues = Mathf.CeilToInt(winningDiceValuePercentage * 100f);
                amountOfValuesTotal -= amountOfWinningValues;
            }
            int amountOfValuesPerNormalValue = (int)(amountOfValuesTotal / (MAX_VALUE - 1));
            //Debug.Log($"Filling dice values, NORMAL values: {amountOfValuesPerNormalValue}, WINNING values {amountOfWinningValues}");
            // Fill the probabilities list with the amount of entries each dice value have
            for (int diceValue = 1; diceValue < MAX_VALUE; diceValue++)
            {
                // Filling the normal values
                for (int i = 0; i < amountOfValuesPerNormalValue; i++)
                {
                    probabilitiesList.Add(diceValue);
                    //Debug.Log($"Filling dice values, normal: diceValue {diceValue}, iteration {i}");
                }
            }
            // Filling the winning values in the list (if any)
            int currentCount = probabilitiesList.Count;
            for (int i = totalEntries; i > currentCount; i--)
            {
                probabilitiesList.Add(MAX_VALUE);
                //Debug.Log($"Filling dice values, WINNING: diceValue {MAX_VALUE}, iteration {i}, count: {probabilitiesList.Count}");
            }
            //Debug.Log($"Filling dice values, all values count: {probabilitiesList.Count}");
        }

        public int GetNewDiceValue()
        {
            int index = UnityEngine.Random.Range(0, probabilitiesList.Count);
            return probabilitiesList[index];
        }

        public void ThrowDiceAnimated(Action<int> onFinish, int forcedValue = -1)
        {
            int newDiceValue = GetNewDiceValue();
            if (forcedValue != -1)
            {
                newDiceValue = forcedValue;
            }
            StartCoroutine(AnimateDiceSequence3(newDiceValue, onFinish));
        }

        private IEnumerator AnimateDiceSequence(int finalDiceValue, Action<int> onFinish)
        {
            IsDiceRolling = true;
            float elapsedTime = 0;
            while (elapsedTime < animationDuration)
            {
                elapsedTime += Time.deltaTime;
                int randomDiceValue = GetNewDiceValue();
                ShowDiceValue(randomDiceValue);
                yield return diceAnimationFrameWaitTime;
            }
            // At the end, show the already gotten dice value
            ShowDiceValue(finalDiceValue);

            IsDiceRolling = false;
            onFinish?.Invoke(finalDiceValue);
        }

        private IEnumerator AnimateDiceSequence2(int finalDiceValue, Action<int> onFinish)
        {
            IsDiceRolling = true;
            diceDisplayed.SetActive(true);
            // Make random number of random rotations
            int rotations = UnityEngine.Random.Range(9, 17);
            for (int i = 0; i < rotations; i++)
            {
                var targetRotation = GetRandomRotation();
                yield return AnimateRotation(targetRotation, animationDuration);
            }
            // At the end, show the already gotten dice value
            var finalRotation = diceValues[finalDiceValue - 1].transform.rotation;
            Debug.Log($"GetRandomRotation: FINAL diceValue: {finalDiceValue}");
            yield return AnimateRotation(finalRotation, animationDuration);
            //ShowDiceValue(finalDiceValue);

            IsDiceRolling = false;
            onFinish?.Invoke(finalDiceValue);
        }

        private IEnumerator AnimateRotation(Quaternion targetRotation, float duration)
        {
            float elapsedTime = 0;
            while (elapsedTime < duration)
            {
                elapsedTime += Time.deltaTime;
                diceDisplayed.transform.rotation = Quaternion.Slerp(diceDisplayed.transform.rotation, targetRotation, Mathf.Clamp01(elapsedTime / duration));
                yield return null;
            }
        }

        private IEnumerator AnimateDiceSequence3(int finalDiceValue, Action<int> onFinish)
        {
            IsDiceRolling = true;
            diceDisplayed.SetActive(true);
            diceAnimator.enabled = true;

            this.diceAnimator.SetInteger("rotation", finalDiceValue);
            //yield return new WaitForSeconds(diceAnimator.GetCurrentAnimatorStateInfo(0).length);
            //this.diceAnimator.SetInteger("rotation", 0);
            yield return new WaitForSeconds(1.3f);
            diceAnimator.enabled = false;
            var finalRotation = diceValues[finalDiceValue - 1].transform.rotation;
            yield return AnimateRotation(finalRotation, animationDuration);
            //diceDisplayed.transform.rotation = finalRotation;

            IsDiceRolling = false;
            onFinish?.Invoke(finalDiceValue);
        }

        private Quaternion GetRandomRotation()
        {
            int randomDiceValue = GetNewDiceValue();
            Debug.Log($"GetRandomRotation: diceValue obtained: {randomDiceValue}");
            var diceFaceObject = diceValues[randomDiceValue - 1];
            return diceFaceObject.transform.rotation;
        }

        public void ShowDiceValue(int faceToShow)
        {
            for (int i = 0; i < diceValues.Count; i++)
            {
                var diceFace = diceValues[i];
                // Only turn on the indicated face
                diceFace.SetActive(faceToShow == (i + 1));
            }
        }

        public void Reset()
        {
            // Hide all the dices
            ShowDiceValue(-1);
        }
    }
}
