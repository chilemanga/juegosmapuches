using KurGames.Mapuches.Kechukawe.Domain.GameFlow;
using UnityEngine;
using TMPro;

namespace KurGames.Mapuches.Kechukawe.Presentation.Player
{
    public class ScoreController : MonoBehaviour
    {
        [SerializeField] private bool isPlayer1 = default;
        [SerializeField] private TextMeshProUGUI scoreValue = default;

        private void Awake()
        {
            GameFlowService.OnGameStart += OnGameStart;
            GameFlowService.OnPlayerTurnSetResults += UpdateScore;
        }

        private void OnDestroy()
        {
            GameFlowService.OnGameStart -= OnGameStart;
            GameFlowService.OnPlayerTurnSetResults -= UpdateScore;
        }

        private void UpdateScore(GamePlayer gamePlayer)
        {
            if ((gamePlayer.IsHumanPlayer && isPlayer1)
                || (!gamePlayer.IsHumanPlayer && !isPlayer1))
            {
                UpdateScore(gamePlayer.CurrentScore);
            }
        }

        private void OnGameStart()
        {
            UpdateScore((int)default);
        }

        public void UpdateScore(int scoreAmount)
        {
            scoreValue.text = scoreAmount.ToString();
        }
    }
}
