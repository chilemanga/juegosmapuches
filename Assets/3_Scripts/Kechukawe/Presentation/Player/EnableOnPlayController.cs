using KurGames.Mapuches.Kechukawe.Domain.GameFlow;
using UnityEngine;

namespace KurGames.Mapuches.Kechukawe.Presentation.Player
{
    public class EnableOnPlayController : MonoBehaviour
    {
        [SerializeField] private GameObject playModeObject = default;

        private void Awake()
        {
            playModeObject.SetActive(false);
            GameFlowService.OnGameStart += OnGameStart;
            GameFlowService.OnGameFinish += OnGameFinish;
        }

        private void OnDestroy()
        {
            GameFlowService.OnGameStart -= OnGameStart;
            GameFlowService.OnGameFinish -= OnGameFinish;
        }

        private void OnGameStart()
        {
            playModeObject.SetActive(true);
        }

        private void OnGameFinish(GamePlayer gamePlayer)
        {
            playModeObject.SetActive(false);
        }
    }
}
