using System;
using System.Collections;
using KurGames.Mapuches.Kechukawe.Domain.GameFlow;
using KurGames.Mapuches.Kechukawe.Presentation.Player;
using KurGames.Mapuches.Common.Presentation.GameFlow;
using UnityEngine;

namespace KurGames.Mapuches.Kechukawe.Presentation.GameFlow
{
    public class GameFlowController : BaseGameFlowController
    {
        [SerializeField] private PlayerController player1Controllers = null;
        [SerializeField] private PlayerController player2Controllers = null;

        private const int SCORE_TO_WIN = 24;
        private const int IMMEDIATE_DICE_WIN = 5;

        protected override void Awake()
        {
            base.Awake();

            GameFlowService.OnPlayerTurnFinish += OnFinishTurn;
            GameFlowService.OnGameFinish += OnGameFinish;
        }

        private void OnDestroy()
        {
            GameFlowService.OnPlayerTurnFinish -= OnFinishTurn;
            GameFlowService.OnGameFinish -= OnGameFinish;
        }

        private void Start()
        {
            var gamePlayerHuman = new GamePlayer(isHumanPlayer: true);
            player1Controllers.SetPlayer(gamePlayerHuman);

            var gamePlayerIA = new GamePlayer(isHumanPlayer: false);
            player2Controllers.SetPlayer(gamePlayerIA);
        }

        private void OnFinishTurn(GamePlayer gamePlayer)
        {
        }

        private void OnGameFinish(GamePlayer gamePlayer)
        {
            // TODO: This is now managed by win or lose popups
            //StartCoroutine(RestartCR());
        }

        public override void StartGame(int id = 1)
        {
            var p1 = player1Controllers.GetPlayer();
            var p2 = player2Controllers.GetPlayer();
            GameFlowService.SetPlayers(new[] { p1, p2 });
            GameFlowService.StartGame(SCORE_TO_WIN, IMMEDIATE_DICE_WIN);
        }

#if UNITY_EDITOR
        private void Update()
        {
            if (Input.GetKey(KeyCode.F))
            {
                Time.timeScale = 10f;
            }
            else
            {
                Time.timeScale = 1f;
            }
        }
#endif
    }
}
