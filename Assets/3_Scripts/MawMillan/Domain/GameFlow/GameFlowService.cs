using System;
using System.Collections.Generic;
using System.Linq;
using KurGames.Mapuches.MawMillan.Domain.Player;
using UnityEngine;
using _3_Scripts.Common.Infrastructure.CoroutineRunner;

namespace KurGames.Mapuches.MawMillan.Domain.GameFlow
{
    // TODO; use zenject instead of an static class...
    public static class GameFlowService
    {
        private static List<GamePlayer> players = new List<GamePlayer>();
        public static GamePlayer CurrentPlayer = null;
        public static event Action OnGameStart = delegate { };
        public static event Action OnMillanScream = delegate { };
        public static event Action<GamePlayer> OnGameFinish = delegate { };

        private static int timeLimit = 60;
        private static DateTime startGameTimestamp;

        public static void SetPlayers(IEnumerable<GamePlayer> newPlayers)
        {
            Debug.Log("<color=orange>GameFlowService: SetPlayers</color>");
            players.Clear();
            CurrentPlayer = null;

            foreach (var player in newPlayers)
            {
                Debug.Log($"GameFlowService: Add Player: [<color=cyan>isHuman:{player.IsHumanPlayer}</color>]");
                players.Add(player);
                if (player.IsHumanPlayer)
                {
                    CurrentPlayer = player;
                }
            }
        }

        public static void StartGame(int timeLimit)
        {
            Debug.Log($"<color=orange>GameFlowService: StartGame, scoreToWin; [{timeLimit}]</color>");
            GameFlowService.timeLimit = timeLimit;
            startGameTimestamp = DateTime.Now;

            foreach (var gamePlayer in players)
            {
                gamePlayer.Restart();
            }

            OnGameStart?.Invoke();
        }

        public static TimeSpan GetRemainingTime()
        {
            var limitInTime = (startGameTimestamp.AddSeconds(timeLimit));
            return limitInTime - DateTime.Now;
        }
        public static bool IsTimeFinished()
        {
            return GetRemainingTime().Seconds <= 0;
        }

        public static GamePlayer GetOponent(GamePlayer gamePlayer)
        {
            foreach (var player in players)
            {
                if (gamePlayer.Type != player.Type)
                {
                    return player;
                }
            }
            return null;
        }

        public static void MillanScream()
        {
            OnMillanScream?.Invoke();
        }

        public static void FinishGame(GamePlayer winner)
        {
            Debug.Log($"<color=yellow>GameFlowService: FinishGame winner: [<color=cyan>isHuman:{winner.IsHumanPlayer}</color>], type:[{winner.Type}]</color>");
            OnGameFinish?.Invoke(winner);
        }
        public static void FinishGameByTime()
        {
            // Only millan wins when time is up
            if (CurrentPlayer.Type == PlayerType.Millan)
            {
                FinishGame(CurrentPlayer);
            }
            else
            {
                FinishGame(GetOponent(CurrentPlayer));
            }
        }
    }
}
