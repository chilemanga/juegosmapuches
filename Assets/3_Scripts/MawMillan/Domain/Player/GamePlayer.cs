namespace KurGames.Mapuches.MawMillan.Domain.Player
{
    public enum PlayerType
    {
        None,
        Maw,
        Millan
    }

    public class GamePlayer
    {
        public bool IsHumanPlayer { get; private set; }
        public float Speed { get; private set; }
        public PlayerType Type { get; private set; }

        public GamePlayer(bool isHumanPlayer, float speed, PlayerType playerType)
        {
            IsHumanPlayer = isHumanPlayer;
            Speed = speed;
            Type = playerType;
            Restart();
        }

        public void Restart()
        {
        }
    }
}
