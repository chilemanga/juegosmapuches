using System;
using System.Collections;
using KurGames.Mapuches.Common.Presentation.GameFlow;
using KurGames.Mapuches.MawMillan.Domain.GameFlow;
using KurGames.Mapuches.MawMillan.Domain.Player;
using KurGames.Mapuches.MawMillan.Presentation.Player;
using UnityEngine;

namespace KurGames.Mapuches.MawMillan.Presentation.GameFlow
{
    public class GameFlowController : BaseGameFlowController
    {
        [SerializeField] private MawController mawController = default;
        [SerializeField] private MillanController millanController = default;
        [SerializeField] private float mawSpeed = 1f;
        [SerializeField] private float millanSpeed = 2f;
        [SerializeField] private int timeLimit = 30;

        public static event Action<PlayerType> OnGameStartAs;

        protected override void Awake()
        {
            base.Awake();
        }

        public override void StartGame(int id = 1)
        {
            bool playerIsMaw = id == 1; // If it's not Maw, it's Millán
            var gamePlayerMaw = new GamePlayer(isHumanPlayer: playerIsMaw, speed: mawSpeed, playerType: PlayerType.Maw);
            mawController.SetPlayer(gamePlayerMaw);

            var gamePlayerMillan = new GamePlayer(isHumanPlayer: !playerIsMaw, speed: millanSpeed, playerType: PlayerType.Millan);
            millanController.SetPlayer(gamePlayerMillan, mawController);

            var p1 = mawController.GetPlayer();
            var p2 = millanController.GetPlayer();
            GameFlowService.SetPlayers(new[] { p1, p2 });

            Debug.Log("MawMillan: Start game!");
            GameFlowService.StartGame(timeLimit);
        }

        public GameObject GetOponentGameObject(GamePlayer player)
        {
            var oponent = GameFlowService.GetOponent(player);
            if (oponent == null)
            {
                Debug.LogError($"Impossible to get oponent for player {player.Type}");
                return null;
            }
            if (oponent.Type == PlayerType.Maw)
            {
                return millanController.gameObject;
            }
            else
            {
                return mawController.gameObject;
            }
        }
    }
}
