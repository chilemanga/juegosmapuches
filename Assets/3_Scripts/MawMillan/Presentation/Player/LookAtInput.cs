using UnityEngine;
using System;

namespace KurGames.Mapuches.MawMillan.Presentation.Player
{
    public class LookAtInput : MonoBehaviour
    {
        public bool IsEnabled = default;

        private Camera mainCamera = null;
        public Vector3? CurrentWard = null;

        private void Awake()
        {
            mainCamera = Camera.main;
        }

        public Vector3 LookAt(Vector3 wardPosition)
        {
            CurrentWard = wardPosition;
            // As the field is 2D, direction is calculated with the difference between mouse position and current position vectors.
            Vector3 direction = new Vector3(wardPosition.x - transform.position.x, wardPosition.y - transform.position.y, transform.position.z);
            transform.right = direction;

            return CurrentWard.Value;
        }

        public Vector3 LookAtOpposite()
        {
            return LookAt(-transform.right * 10f);
        }

        private void Update()
        {
            if (!IsEnabled)
            {
                return;
            }
            if (Input.GetMouseButton(0))
            {
                LookAt(mainCamera.ScreenToWorldPoint(Input.mousePosition));
            }
        }
    }
}
