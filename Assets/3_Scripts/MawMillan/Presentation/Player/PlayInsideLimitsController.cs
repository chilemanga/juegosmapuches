using UnityEngine;
using System;

namespace KurGames.Mapuches.MawMillan.Presentation.Player
{
    public class PlayInsideLimitsController : MonoBehaviour
    {
        private Action onTriggerEnter = default;

        public void SubscribeToTriggerEnterWithLimits(Action onTriggerEnter)
        {
            this.onTriggerEnter = onTriggerEnter;
        }
        public void UnsubscribeToTriggerEnterWithLimits()
        {
            this.onTriggerEnter = null;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other == null || onTriggerEnter == null)
            {
                return;
            }
            // TODO: Differentiate if is human or not
            if (other.CompareTag("FieldLimit"))
            {
                onTriggerEnter?.Invoke();
            }
        }
    }
}
