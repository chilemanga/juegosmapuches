using UnityEngine;
using KurGames.Mapuches.MawMillan.Domain.Player;
using KurGames.Mapuches.MawMillan.Presentation.GameFlow;
using KurGames.Mapuches.MawMillan.Domain.GameFlow;
using KurGames.Mapuches.Common.Utils;

namespace KurGames.Mapuches.MawMillan.Presentation.Player
{
    [RequireComponent(typeof(PlayInsideLimitsController))]
    public class MawController : MonoBehaviour
    {
        [SerializeField] private GameFlowController gameFlowController = default;
        [SerializeField] private GameObject screamGO = default;
        [SerializeField] private GameObject maskGO = default;
        [SerializeField] private LookAtInput lookAtInput = default;
        [SerializeField] private GameObject carrotToLookAt = default;
        [SerializeField] private float timeShowingScream = 1f;
        [SerializeField] private SpriteColorChanger spriteColorChanger = default;

        public event System.Action OnScreamMaw;

        private PlayInsideLimitsController limitsController = null;
        private GamePlayer gamePlayer = null;
        private Camera mainCamera = null;
        private bool gameStarted = false;

        private float randomWaitingTime
        {
            get
            {
                return 1f + (Random.value * 1.7f);
            }
        }
        private float iaElapsedTime = 0;
        private float thisLoopRandomTime;

        private void Awake()
        {
            mainCamera = Camera.main;
            limitsController = GetComponent<PlayInsideLimitsController>();
            limitsController.SubscribeToTriggerEnterWithLimits(OnTouchLimits);
            GameFlowService.OnGameStart += OnGameStart;
            MawButtonController.OnMawScream += Scream;
            GameFlowService.OnMillanScream += OnMillanScream;
            GameFlowService.OnGameFinish += OnGameFinish;
        }

        private void OnDestroy()
        {
            GameFlowService.OnGameStart -= OnGameStart;
            MawButtonController.OnMawScream -= Scream;
            GameFlowService.OnMillanScream -= OnMillanScream;
            GameFlowService.OnGameFinish -= OnGameFinish;
            limitsController.UnsubscribeToTriggerEnterWithLimits();
        }

        public void SetPlayer(GamePlayer player)
        {
            gamePlayer = player;
            maskGO.SetActive(player.IsHumanPlayer);
            lookAtInput.IsEnabled = player.IsHumanPlayer;
        }

        private void OnGameStart()
        {
            gameStarted = true;
        }
        private void OnGameFinish(GamePlayer winner)
        {
            gameStarted = false;
        }

        public GamePlayer GetPlayer()
        {
            return gamePlayer;
        }

        public void Scream()
        {
            screamGO.SetActive(true);
            OnScreamMaw?.Invoke();
            Invoke(nameof(HideScream), 1f);
            if (!GetPlayer().IsHumanPlayer)
            {
                maskGO.SetActive(true);
                Invoke(nameof(HideMask), 1f);
            }
        }

        public void HideScream()
        {
            screamGO.SetActive(false);
        }
        public void HideMask()
        {
            maskGO.SetActive(false);
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other == null)
            {
                return;
            }
            MillanController millan = other.GetComponent<MillanController>();
            if (millan != null)
            {
                // Win!
                Debug.Log("Millán was caught! Maw win!!");
                GameFlowService.FinishGame(this.GetPlayer());
                return;
            }
        }

        private void OnTouchLimits()
        {
            Debug.Log("Maw is touching field limits!!!");
            // Touched limits? Return to the position it came, for 1 second
            //lookAtInput.LookAt(new Vector3(-transform.position.x, -transform.position.y, transform.position.z));
            lookAtInput.LookAtOpposite();
            if (spriteColorChanger != null)
            {
                spriteColorChanger.SetState(SpriteColorState.Highlighted, 1f);
            }
            Invoke(nameof(StopMoving), 1f);
        }

        private void OnMillanScream()
        {
            if (!GetPlayer().IsHumanPlayer)
            {
                iaElapsedTime = 0;
                thisLoopRandomTime = randomWaitingTime;
                var millan = gameFlowController.GetOponentGameObject(GetPlayer());
                //lookAtInput.LookAt(new Vector3(millan.transform.position.x, millan.transform.position.y, transform.position.z));
                lookAtInput.LookAt(millan.transform.position);
            }
        }

        // TODO: Use Lean to capture touches instead of mouse
        private void Update()
        {
            if (!gameStarted)
            {
                return;
            }

            if (!GetPlayer().IsHumanPlayer)
            {
                // TODO: for now, automatically follow Millán
                //var oponent = gameFlowController.GetOponentGameObject(GetPlayer());
                //LookAt(oponent.transform.position);

                // Just pick a random rotation and go
                if (iaElapsedTime == 0)
                {
                    thisLoopRandomTime = randomWaitingTime;
                }
                iaElapsedTime += Time.deltaTime;
                if (iaElapsedTime >= thisLoopRandomTime)
                {
                    transform.Rotate(new Vector3(transform.position.x, transform.position.y, Random.value * 360f));
                    lookAtInput.LookAt(carrotToLookAt.transform.position);
                    Scream();
                    iaElapsedTime = 0;
                    thisLoopRandomTime = randomWaitingTime;
                }
            }

            if (CanMove())
            {
                MoveForward();
            }
            else
            {
                StopMoving();
            }
        }

        public bool CanMove()
        {
            float distance = lookAtInput.CurrentWard.HasValue ? Vector2.Distance(lookAtInput.CurrentWard.Value, transform.localPosition) : 0;
            if (lookAtInput.CurrentWard.HasValue)
            {
                Debug.Log($"distance between ward {lookAtInput.CurrentWard.Value} and pos {transform.localPosition} is {distance}");
            }
            return lookAtInput.CurrentWard.HasValue && distance > 0.1f;

            // TODO: Avoid to leave circle
            //float boundaryDistance = Vector2.Distance(Vector2.zero, transform.localPosition);
            //Debug.Log($"Boundary distance is {boundaryDistance}, pos: {transform.localPosition}");
            //// if it's too close to current position, it can't move
            //return currentWard.HasValue && distance > 0.1f && boundaryDistance <= 8f;
        }

        public void MoveForward()
        {
            if (!lookAtInput.CurrentWard.HasValue) return;

            transform.Translate(Vector3.right * GetPlayer().Speed * Time.deltaTime);
        }

        public void StopMoving()
        {
            lookAtInput.CurrentWard = null;
        }
    }
}
