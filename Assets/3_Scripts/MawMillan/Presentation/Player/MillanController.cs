using UnityEngine;
using KurGames.Mapuches.MawMillan.Domain.Player;
using KurGames.Mapuches.MawMillan.Domain.GameFlow;
using KurGames.Mapuches.Common.Utils;

namespace KurGames.Mapuches.MawMillan.Presentation.Player
{
    [RequireComponent(typeof(PlayInsideLimitsController))]
    public class MillanController : MonoBehaviour
    {
        [SerializeField] private GameObject screamGO = default;
        [SerializeField] private GameObject maskGO = default;
        [SerializeField] private LookAtInput lookAtInput = default;
        [SerializeField] private SpriteColorChanger spriteColorChanger = default;

        private const float timeBeingShown = 0.5f;
        private const float delayBeforeShow = 1f;

        private MawController maw;

        private PlayInsideLimitsController limitsController = null;
        private GamePlayer gamePlayer = null;
        private Camera mainCamera = null;
        private bool gameStarted = false;

        private void Awake()
        {
            mainCamera = Camera.main;
            limitsController = GetComponent<PlayInsideLimitsController>();
            limitsController.SubscribeToTriggerEnterWithLimits(OnTouchLimits);
            GameFlowService.OnGameStart += OnGameStart;
            GameFlowService.OnGameFinish += OnGameFinish;
        }

        private void OnDestroy()
        {
            GameFlowService.OnGameStart -= OnGameStart;
            GameFlowService.OnGameFinish -= OnGameFinish;
            if (this.maw != null)
            {
                this.maw.OnScreamMaw -= OnMawScream;
                this.maw = null;
            }
            limitsController.UnsubscribeToTriggerEnterWithLimits();
        }

        public void SetPlayer(GamePlayer player, MawController maw)
        {
            gamePlayer = player;
            maskGO.SetActive(player.IsHumanPlayer);
            lookAtInput.IsEnabled = player.IsHumanPlayer;
            this.maw = maw;
            this.maw.OnScreamMaw += OnMawScream;
        }

        private void OnGameStart()
        {
            gameStarted = true;
        }
        private void OnGameFinish(GamePlayer winner)
        {
            gameStarted = false;
        }

        public GamePlayer GetPlayer()
        {
            return gamePlayer;
        }

        public void OnMawScream()
        {
            Invoke(nameof(Show), delayBeforeShow);
            LookAwayFromMaw();
        }

        public void Show()
        {
            maskGO.SetActive(true);
            screamGO.SetActive(true);
            GameFlowService.MillanScream();
            Invoke(nameof(Hide), timeBeingShown);
        }
        public void Hide()
        {
            maskGO.SetActive(GetPlayer().IsHumanPlayer);
            screamGO.SetActive(false);
        }

        public void LookAwayFromMaw()
        {
            // As the field is 2D, direction is calculated with the difference between mouse position and current position vectors.
            //Vector3 direction = new Vector3(transform.position.x - maw.transform.position.x, transform.position.y - maw.transform.position.y, transform.position.z);
            //LookAt(direction);

            // TODO: Look away from maw
            //LookAt(-maw.transform.position);
            //lookAtInput.LookAt(-maw.transform.position);
        }

        private void OnTouchLimits()
        {
            Debug.Log("Millan is touching field limits!!!");
            // Touched limits? Return to the position it came, for 1 second
            //var direction = new Vector3(-transform.position.x, -transform.position.y, transform.position.z);
            //lookAtInput.LookAt(direction);
            lookAtInput.LookAtOpposite();
            if (spriteColorChanger != null)
            {
                spriteColorChanger.SetState(SpriteColorState.Highlighted, 1f);
            }
            Invoke(nameof(StopMoving), 1f);
        }

        // TODO: Use Lean to capture touches instead of mouse
        private void Update()
        {
            if (!gameStarted)
            {
                return;
            }

            //if (!GetPlayer().IsHumanPlayer)
            //{
            //    LookAwayFromMaw();
            //}

            MoveForward();
        }

        public void MoveForward()
        {
            transform.Translate(Vector3.right * GetPlayer().Speed * Time.deltaTime);
        }

        public void StopMoving()
        {
            lookAtInput.CurrentWard = null;
        }
    }
}
