﻿using System;
using KurGames.Mapuches.MawMillan.Domain.GameFlow;
using UnityEngine;

namespace KurGames.Mapuches.MawMillan.Presentation.Player
{
    public class MawButtonController : MonoBehaviour
    {
        [SerializeField] private GameObject buttonGO = default;

        public static Action OnMawScream = null;

        private void Awake()
        {
            GameFlowService.OnGameStart += OnGameStart;
        }

        private void OnDestroy()
        {
            GameFlowService.OnGameStart -= OnGameStart;
        }

        private void OnGameStart()
        {
            buttonGO.SetActive(GameFlowService.CurrentPlayer.Type == Domain.Player.PlayerType.Maw);
        }

        public void OnMawButtonPressed()
        {
            OnMawScream?.Invoke();
        }

#if UNITY_EDITOR
        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.A))
            {
                OnMawButtonPressed();
            }
        }
#endif
    }
}
