﻿using KurGames.Mapuches.Common.Presentation.UI;
using KurGames.Mapuches.MawMillan.Domain.GameFlow;
using KurGames.Mapuches.MawMillan.Domain.Player;

namespace KurGames.Mapuches.MawMillan.Presentation.UI
{
    public class PopupManager : BasePopupManager
    {
        protected override void Awake()
        {
            base.Awake();
            GameFlowService.OnGameFinish += OnGameFinish;
        }

        protected void OnDestroy()
        {
            GameFlowService.OnGameFinish -= OnGameFinish;
        }

        private void OnGameFinish(GamePlayer gamePlayer)
        {
            base.OnGameFinish(gamePlayer.IsHumanPlayer);
        }
    }
}
