﻿using KurGames.Mapuches.Common.Presentation.UI;
using KurGames.Mapuches.MawMillan.Domain.GameFlow;
using KurGames.Mapuches.MawMillan.Domain.Player;
using UnityEngine;
using TMPro;

namespace KurGames.Mapuches.MawMillan.Presentation.UI
{
    public class TimerController : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI timerText;

        private bool gameStarted = false;
        private bool stopTimer = false;

        private void Awake()
        {
            GameFlowService.OnGameStart += OnGameStart;
            GameFlowService.OnGameFinish += OnGameFinish;
        }

        private void OnGameStart()
        {
            gameStarted = true;
            stopTimer = false;
        }

        private void Update()
        {
            if (!gameStarted || stopTimer)
            {
                return;
            }

            UpdateTimer();
            if (GameFlowService.IsTimeFinished())
            {
                GameFlowService.FinishGameByTime();
                StopTimer();
            }
        }

        private void UpdateTimer()
        {
            timerText.text = "Tiempo Restante: " + GameFlowService.GetRemainingTime().Seconds.ToString("00");
        }

        private void OnGameFinish(GamePlayer gamePlayer)
        {
            StopTimer();
        }

        public void StopTimer()
        {
            stopTimer = true;
        }
    }
}