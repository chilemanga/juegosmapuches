﻿using System;
using KurGames.Mapuches.Common.Presentation.UI;
using KurGames.Mapuches.MawMillan.Domain.GameFlow;
using KurGames.Mapuches.MawMillan.Domain.Player;
using UnityEngine;
using TMPro;

namespace KurGames.Mapuches.MawMillan.Presentation.UI
{
    public class PlayerUIController : MonoBehaviour
    {
        [SerializeField] private Canvas canvas;

        private void Awake()
        {
            canvas.enabled = false;
            GameFlowService.OnGameStart += OnGameStart;
            GameFlowService.OnGameFinish += OnGameFinish;
        }

        private void OnDestroy()
        {
            GameFlowService.OnGameStart -= OnGameStart;
            GameFlowService.OnGameFinish -= OnGameFinish;
        }

        private void OnGameStart()
        {
            canvas.enabled = true;
        }

        private void OnGameFinish(GamePlayer gamePlayer)
        {
            canvas.enabled = false;
        }
    }
}
