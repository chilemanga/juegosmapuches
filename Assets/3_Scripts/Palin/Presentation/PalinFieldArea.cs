using _3_Scripts.Palin.Domain.Game;
using UnityEngine;

namespace _3_Scripts.Palin.Presentation
{
    public class PalinFieldArea : MonoBehaviour
    {
        [SerializeField]
        private PlayableArea playableArea = PlayableArea.Undefined;

        public void OnTriggerEnter(Collider other)
        {
            if (!other.CompareTag("PalinBall"))
            {
                return;
            }

            PalinGameService.NotifyActiveArea(playableArea);
        }
    }
}
