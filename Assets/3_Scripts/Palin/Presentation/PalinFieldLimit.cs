using _3_Scripts.Palin.Domain.Game;
using UnityEngine;

namespace _3_Scripts.Palin.Presentation
{
    public class PalinFieldLimit : MonoBehaviour
    {
        private void Awake()
        {
            PalinGameService.RegisterFieldLimit(transform.position);
        }
    }
}
