using System;
using System.Collections;
using _3_Scripts.Palin.Domain.Game;
using KurGames.Mapuches.Common.Presentation.GameFlow;
using UnityEngine;

namespace _3_Scripts.Palin.Presentation
{
    public class PalinGameFlowController : BaseGameFlowController
    {
        [SerializeField] private GameObject gameUI = null;

        [SerializeField] private GameObject playerWinUI = null;
        [SerializeField] private GameObject playerLoseUI = null;

        private new void Awake()
        {
            base.Awake();
            PalinGameService.OnPlayerWin += OnPlayerWin;
            PalinGameService.OnPlayerLose += OnPlayerLose;
        }

        private void OnDestroy()
        {
            PalinGameService.OnPlayerWin -= OnPlayerWin;
            PalinGameService.OnPlayerLose -= OnPlayerLose;
        }

        public override void StartGame(int id = 1)
        {
            // restart ui
            playerWinUI.SetActive(false);
            playerLoseUI.SetActive(false);

            var waitTime = 0.5f;
            StartCoroutine(DelayedStart(waitTime));
        }

        private IEnumerator DelayedStart(float waitTime)
        {
            yield return new WaitForSeconds(waitTime);

            gameUI.SetActive(true);

            PalinGameService.StartGame();
        }

        private void OnPlayerWin()
        {
            playerWinUI.SetActive(true);
        }

        private void OnPlayerLose()
        {
            playerLoseUI.SetActive(true);
        }

    }
}
