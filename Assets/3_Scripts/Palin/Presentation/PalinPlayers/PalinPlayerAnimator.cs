using UnityEngine;

namespace _3_Scripts.Palin.Presentation.PalinPlayers
{
    public class PalinPlayerAnimator : MonoBehaviour
    {
        [SerializeField] private string runTrigger = "run";
        [SerializeField] private string shootTrigger = "shoot";
        [SerializeField] private string idleTrigger = "idle";
        [SerializeField]
        private Animator animator = null;

        private int Run, Shoot, Idle1 = default;

        private void Awake()
        {
            Run = Animator.StringToHash(runTrigger);
            Shoot = Animator.StringToHash(shootTrigger);
            Idle1 = Animator.StringToHash(idleTrigger);
        }


        public void PursueBall()
        {
            animator.SetTrigger(Run);
        }

        public void PassBall()
        {
            animator.SetTrigger(Shoot);
        }

        public void Attack()
        {
            animator.SetTrigger(Run);
        }

        public void CaptureBall()
        {
            animator.SetTrigger(Run);
        }

        public void Patrol()
        {
            animator.SetTrigger(Run);
        }

        public void Defend()
        {
            animator.SetTrigger(Run);
        }

        public void Idle()
        {
            animator.SetTrigger(Idle1);
        }

        public void MoveToInitialPosition()
        {
            animator.SetTrigger(Run);
        }


    }
}
