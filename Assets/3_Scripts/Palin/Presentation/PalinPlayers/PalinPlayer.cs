using System.Linq;
using _3_Scripts.Common.Infrastructure.FSM;
using _3_Scripts.Common.Infrastructure.Steering;
using _3_Scripts.Palin.Domain.Game;
using UnityEngine;

namespace _3_Scripts.Palin.Presentation.PalinPlayers
{
    public class PalinPlayer : MonoBehaviour, IBoid
    {
        private const float BALL_CLOSE_ENOUGH_TO_START_PURSUE = 4f;
        private const float BALL_TOO_FAR_TO_KEEP_PURSUING = 10f;
        private const float MAX_SPEED = 0.10f;

        [SerializeField]
        private PalinPlayerAnimator playerAnimator = null;
        [SerializeField]
        private GameObject ballOwnerIndicator = null;
        [SerializeField]
        private PlayerType playerType = PlayerType.Undefined;

        [SerializeField]
        private PlayableArea playerArea = PlayableArea.Undefined;

        public Vector3 Velocity => transform.forward.normalized * MaxSpeed;

        public Vector3 Position
        {
            get => transform.position;
            private set => transform.position = value;
        }
        public float MaxSpeed => MAX_SPEED;

        public Team playerTeam = Team.Undefined;

        private MovementManager movementManager = null;
        private StackFSM fsm = null;
        private Vector3 initialPosition = default;

        private void Awake()
        {
            initialPosition = transform.position;
            Debug.Log($"initialPosition: [{initialPosition}]");

            movementManager = new MovementManager(this, 0.25f, 0.03f);
            fsm = new StackFSM();

            PalinGameService.RegisterPlayer(this);
        }

        private void Start()
        {
            fsm.PushState(PrepareForMatch);
        }


        private void PursueBall()
        {
            playerAnimator.PursueBall();

            var ball = PalinGameService.GetBall();

            var isBallTooFar = Vector3.Distance(ball.Position, Position) > BALL_TOO_FAR_TO_KEEP_PURSUING;

            if (isBallTooFar)
            {
                fsm.PopState();
                Debug.Log($"Player:[{name}]; from pursue-> push: [Idle]");
                fsm.PushState(Idle);
                return;
            }

            var currentPlayableArea = PalinGameService.CurrentActiveArea;
            if (currentPlayableArea != playerArea)
            {
                fsm.PopState();
                fsm.PushState(PrepareForMatch);
                return;
            }

            var ballOwner = ball.GetOwner();
            if (ballOwner == null)
            {
                LookAt(ball.Position);
                Debug.Log($"Player:[{name}]; from pursue-> movement: seek!");
                movementManager.Seek(ball.Position, 1f);
            }
            else
            {
                fsm.PopState();

                if (ballOwner.playerTeam == playerTeam)
                {
                    Debug.Log($"Player:[{name}]; from pursue-> push: [Attack]");
                    fsm.PushState(Attack);
                }
                else
                {
                    Debug.Log($"Player:[{name}]; from pursue-> push: [Defend]");
                    fsm.PushState(Defend);
                }
            }

        }


        private void ThrowOutTheBall()
        {
            Debug.Log($"Player:[{name}]; try to xpulseTheBall! ");
            playerAnimator.PassBall();
            var ball = PalinGameService.GetBall();

            // closest border
            var destination = PalinGameService.GetClosestBorderPosition(ball.Position);

            ball.GetHitFromPlayer(this, destination);

            fsm.PopState();
        }

        private void PassBall()
        {
            Debug.Log($"Player:[{name}]; try to PassBall! ");
            playerAnimator.PassBall();

            // hit direction is always forward....
            var ball = PalinGameService.GetBall();
            var closestTeammate = PalinGameService.GetClosestPlayerFromBall(this, true, true);

            if (closestTeammate != null)
            {
                playerAnimator.PassBall();


                Debug.Log($"Player:[{name}]; PassBall - pass to [{closestTeammate.name}]...");
                ball.GetHitFromPlayer(this, closestTeammate.Position);

                fsm.PopState();
            }
        }


        private void ShootBall()
        {
            Debug.Log($"Player:[{name}]; try to PassBall! ");
            playerAnimator.PassBall();

            // hit direction is always forward....
            var ball = PalinGameService.GetBall();
            var destination = transform.forward * 1000f;
            ball.GetHitFromPlayer(this, destination);

            fsm.PopState();
        }

        private void ControlledByUserPassBall()
        {
            ShootBall();
            fsm.PushState(Attack);
        }

        private void Attack()
        {

            var ball = PalinGameService.GetBall();
            var owner = ball.GetOwner();

            if (owner == null)
            {
                fsm.PopState();
                Debug.Log($"Player:[{name}]; push: [PursueBall]");
                fsm.PushState(PursueBall);
                return;
            }

            var ownerIsOnTheOtherTeam = owner.playerTeam != playerTeam;

            if (ownerIsOnTheOtherTeam && ball.CanChangeOwner())
            {
                fsm.PopState();
                Debug.Log($"Player:[{name}]; push: [CaptureBall]");
                fsm.PushState(CaptureTheBall);
                return;
            }
            // my team has the ball...

            if (AmITheBallOwner())
            {
                if (playerType == PlayerType.Defender)
                {
                    fsm.PushState(ThrowOutTheBall);
                    return;
                }

                // My team has the ball and I am the one who has it! Let's move
                // towards the opponent's goal, avoding any opponents along the way.
                // If any opponent tries to steal the ball, we pass it to a teammate.
                var opponentsGoal = PalinGameService.GetOpponentGoalPosition(playerTeam);

                LookAt(opponentsGoal);
                playerAnimator.Attack();
                movementManager.Seek(opponentsGoal, 0f);

                const float AVOIDANCE_RADIUS = 0.8f;
                const float MAX_AVOIDANCE_AHEAD = 1f;
                const float AVOIDANCE_FORCE = 0.0f;
                var opponentsTeam = PalinGameService.GetOpponentTeam(this);
                var opponents = PalinGameService.GetPlayersFromTeam(opponentsTeam);
                movementManager.CollisionAvoidance(this, opponents.ToList<IBoid>(), AVOIDANCE_RADIUS, MAX_AVOIDANCE_AHEAD, AVOIDANCE_FORCE);

                if (AnyOpponentsTryingToStealTheBall())
                {
                    var canPassBall = ball.CanChangeOwner();
                    if (canPassBall)
                    {
                        fsm.PushState(PassBall);
                    }
                }

                const float CLOSE_DISTANCE_TO_GOAL = 2f;
                var closeEnoughToGoal = Vector3.Distance(Position, opponentsGoal) < CLOSE_DISTANCE_TO_GOAL;
                if (closeEnoughToGoal)
                {
                    // shoot
                    fsm.PushState(ShootBall);
                }
            }
            else
            {

                var currentPlayableArea = PalinGameService.CurrentActiveArea;
                if (currentPlayableArea != playerArea)
                {
                    fsm.PopState();
                    fsm.PushState(PrepareForMatch);
                    return;
                }

                // My team has the ball, but a teammate has it. Is he ahead of me?
                if (IsAheadOfMe(owner))
                {
                    // Yeah, he is ahead of me. Let's just follow him to give some support
                    // during the attack.
                    LookAt(owner.Position);
                    playerAnimator.Attack();
                    movementManager.FollowLeader(owner);
                }
                else
                {
                    playerAnimator.Attack();
                    movementManager.Evade(ball, 7f);
                    return;
                }


                // we also keep some separation from the
                // other, so we prevent crowding.
                var allPlayers = PalinGameService.GetAllPlayers();
                movementManager.Separation(allPlayers, 1.0f, 0.1f);
            }
        }

        private bool AnyOpponentsTryingToStealTheBall()
        {
            const float AHEAD_PROJECTION = 2f;
            var ahead = Position + Velocity.normalized * AHEAD_PROJECTION;

            var opponentTeam = PalinGameService.GetOpponentTeam(this);

            const float DANGEROUS_ZONE_RADIUS = 2f;
            var totalOpponentsOnProximity = PalinGameService.CountPlayersWithinRadius(opponentTeam, ahead, DANGEROUS_ZONE_RADIUS);

            return totalOpponentsOnProximity > 0;
        }


        private bool IsAheadOfMe(IBoid other)
        {
            var myGoal = PalinGameService.GetOpponentGoalPosition(playerTeam);
            var myDistanceFromGoal = Vector3.Distance(myGoal, Position);
            var othersDistanceFromGoal = Vector3.Distance(myGoal, other.Position);

            return othersDistanceFromGoal < myDistanceFromGoal;
        }

        private bool AmITheBallOwner()
        {
            var ball = PalinGameService.GetBall();
            var owner = ball.GetOwner();

            return owner == this;
        }

        private void CaptureTheBall()
        {
            playerAnimator.CaptureBall();

            var ball = PalinGameService.GetBall();
            var owner = ball.GetOwner();

            if (owner == null)
            {
                fsm.PopState();
                Debug.Log($"Player:[{name}]; from Capture--> push: [PursueBall]");
                fsm.PushState(PursueBall);
                return;
            }

            var ownerIsOnMyTeam = owner.playerTeam == playerTeam;

            if (ownerIsOnMyTeam)
            {
                fsm.PopState();
                Debug.Log($"Player:[{name}]; from Capture--> push: [Attack]");
                fsm.PushState(Attack);
                return;
            }

            // An opponent has the ball.

            var distanceFromOwner = Vector3.Distance(Position, owner.Position);
            // Is the opponent with the ball too far from me?
            if (distanceFromOwner >= BALL_TOO_FAR_TO_KEEP_PURSUING)
            {
                // yes, he is too far away. Let's switch to 'defend' and hope
                // someone closer to the ball can steal it for us.
                fsm.PopState();
                Debug.Log($"Player:[{name}]; from Capture--> push: [Defend]");
                fsm.PushState(Defend);
                return;
            }

            // Opponent is close enough! Let's pursue him while maintaining a certain
            // separation from the others to avoid that everybody will occupy the same
            // position in the pursuit.
            LookAt(ball.Position);
            movementManager.Pursuit(ball);
            var allPlayers = PalinGameService.GetAllPlayers();
            const float OPPONENT_SEPARATION = 1f;
            const float MAX_SEPARATION = 0.1f;
            movementManager.Separation(allPlayers, OPPONENT_SEPARATION, MAX_SEPARATION);
        }

        private void Patrol()
        {
            playerAnimator.Patrol();

            Debug.Log($"Player:[{name}]; on; patrol -->> [wonder]");

            var ball = PalinGameService.GetBall();
            LookAt(ball.Position);

            movementManager.Wander();

            var distanceFromInitial = DistanceFromInitial();
            if (distanceFromInitial < 2f)
            {
                Debug.Log($"Player:[{name}]; patrol ---- continue... : [{distanceFromInitial}] (distanceFromInitial < 2f)");
                return;
            }

            fsm.PopState();
            Debug.Log($"Player:[{name}]; from patrol -> push: [Defend]");
            fsm.PushState(Defend);
        }

        private float DistanceFromInitial()
        {
            return Vector3.Distance(Position, initialPosition);
        }

        private void Defend()
        {
            playerAnimator.Defend();

            movementManager.Arrive(initialPosition, 1f);

            var ball = PalinGameService.GetBall();
            var owner = ball.GetOwner();

            if (owner == null)
            {
                fsm.PopState();
                Debug.Log($"Player:[{name}]; from defend -> push: [PursueBall]");
                fsm.PushState(PursueBall);
                return;
            }

            // TODO SM; patrol is not working very good when defending...
            // var distanceFromInitial = DistanceFromInitial();
            //
            // if (distanceFromInitial < 1f)
            // {
            //     fsm.PopState();
            //     Debug.Log($"Player:[{name}];from defend -> push: [Patrol]");
            //     fsm.PushState(Patrol);
            //     return;
            // }

            var isOwnerInMyTeam = owner.playerTeam == playerTeam;
            if (isOwnerInMyTeam)
            {
                // My team has the ball, time to stop defending and start attacking!
                fsm.PopState();
                Debug.Log($"Player:[{name}];from defend -> push: [Attack]");
                fsm.PushState(Attack);
                return;
            }

            var currentPlayableArea = PalinGameService.CurrentActiveArea;
            if (currentPlayableArea != playerArea)
            {
                fsm.PopState();
                fsm.PushState(PrepareForMatch);
                return;
            }

            var distanceFromOwner = Vector3.Distance(Position, owner.Position);
            if (distanceFromOwner > 10f)
            {
                Debug.Log($"Player:[{name}]; defend ---- continue... : [{distanceFromOwner}] (distanceFromOwner > 5)");
                return;
            }

            // An opponent has the ball and he is close to us!
            // Let's try to steal the ball from him.
            fsm.PopState();
            Debug.Log($"Player:[{name}];from defend -> push: [CaptureTheBall]");
            fsm.PushState(CaptureTheBall);
        }

        private void Idle()
        {
            playerAnimator.Idle();

            var ball = PalinGameService.GetBall();
            LookAt(ball.Position);

            var ballOwner = ball.GetOwner();
            if (ballOwner != null)
            {
                fsm.PopState();

                var ballOwnerTeam = ballOwner.playerTeam;
                if (ballOwnerTeam == playerTeam)
                {
                    Debug.Log($"Player:[{name}]; push: [Attack]");
                    fsm.PushState(Attack);
                }
                else
                {
                    Debug.Log($"Player:[{name}]; push: [Defend]");
                    fsm.PushState(Defend);
                }
            }
            else if (BallIsCloseEnoughToPursue())
            {
                fsm.PopState();
                fsm.PushState(PursueBall);
            }
        }


        private bool BallIsCloseEnoughToPursue()
        {
            var ball = PalinGameService.GetBall();
            return Vector3.Distance(Position, ball.Position) < BALL_CLOSE_ENOUGH_TO_START_PURSUE;
        }

        private void PrepareForMatch()
        {
            var distanceFromInitial = DistanceFromInitial();
            if (distanceFromInitial <= 1f)
            {
                fsm.PopState();
                Debug.Log($"Player:[{name}]; push: [Idle]");
                fsm.PushState(Idle);
            }

            movementManager.Arrive(initialPosition, 5f);
            playerAnimator.MoveToInitialPosition();
        }

        private void LookAt(Vector3 targetPosition)
        {
            var directionToLook = (targetPosition - Position).normalized;

            var angleDiff = Vector3.SignedAngle(transform.forward, directionToLook, Vector3.up);

            var softRotation = Mathf.Clamp(Mathf.Abs(angleDiff), 0, 10f) * Mathf.Sign(angleDiff);

            transform.Rotate(Vector3.up, softRotation);
        }


        private void Update()
        {
            if (!PalinGameService.GameRunning)
            {
                var ball = PalinGameService.GetBall();
                LookAt(ball.Position);
                ballOwnerIndicator.SetActive(false);
                return;
            }

            movementManager.ResetCalculatedForce();

            var controlledByUser = PalinGameService.CheckIfIsControlledByUser(this);
            if (ballOwnerIndicator != null)
            {
                ballOwnerIndicator.SetActive(controlledByUser);
            }

            if (controlledByUser)
            {
                UsePlayerInput();
                return;
            }

            fsm.Tick();
            Position += movementManager.GetCalculatedForce();
        }

        private void UsePlayerInput()
        {
            // TODO: Replace this with joystick movement
            var movement = Vector3.zero;
            if (Input.GetKey(KeyCode.UpArrow))
            {
                movement += Vector3.left;
            }
            else if (Input.GetKey(KeyCode.DownArrow))
            {
                movement += Vector3.right;
            }

            if (Input.GetKey(KeyCode.LeftArrow))
            {
                movement += Vector3.back;
            }
            else if (Input.GetKey(KeyCode.RightArrow))
            {
                movement += Vector3.forward;
            }

            Position += movement * MaxSpeed;

            if (movement != default)
            {
                playerAnimator.Attack();
                LookAt(Position + movement * 10f);
            }
            else
            {
                playerAnimator.Idle();
            }

            if (Input.GetKeyDown(KeyCode.Space))
            {
                playerAnimator.PassBall();
                ControlledByUserPassBall();
            }

        }

        private void WanderInField()
        {
            movementManager.Wander();
        }


        private PalinPlayer GetTeamLeader()
        {
            var ball = PalinGameService.GetBall();
            var owner = ball.GetOwner();
            if (owner != null && owner.playerTeam == playerTeam)
            {
                return owner;
            }

            return null;
        }

        public float GetOwnershipChances()
        {
            // TODO SM; this can be used for difficulty...
            return 1f;
        }

        public void Reset()
        {
            Debug.Log($"Player:[{name}]; push: [PrepareForMatch]");
            fsm.Clean();
            movementManager.Reset();

            Position = initialPosition;

            fsm.PushState(PrepareForMatch);
        }
    }
}
