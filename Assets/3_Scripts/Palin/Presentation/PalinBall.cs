using System;
using _3_Scripts.Common.Infrastructure.Steering;
using _3_Scripts.Palin.Domain.Game;
using _3_Scripts.Palin.Presentation.PalinPlayers;
using UnityEngine;

namespace _3_Scripts.Palin.Presentation
{
    public class PalinBall : MonoBehaviour, IBoid
    {
        public Vector3 Velocity { get; private set; }
        public float MaxSpeed => 0.2f;

        public Vector3 Position {
            get => transform.position;
            private set => transform.position = value;
        }

        private Vector3 initialPosition = default;
        private PalinPlayer owner = null;
        private PalinPlayer oldOwner = null;
        private float immuneTime = default;

        private void Awake()
        {
            initialPosition = transform.position;
            PalinGameService.RegisterBall(this);
        }

        private void Update()
        {
            if (!PalinGameService.GameRunning)
            {
                return;
            }

            if (owner != null)
            {
                PlaceAheadOfOwner();
            }
            else if (Velocity.sqrMagnitude > 0f)
            {
                var littleDisplacement = (Velocity / Velocity.magnitude) * 0.1f;
                Position += littleDisplacement;
            }

            if (immuneTime > 0f)
            {
                immuneTime -= Time.deltaTime;
            }

            PalinGameService.ReportBallPosition(this);
        }

        public void SetOwner(PalinPlayer newOwner)
        {
            if (newOwner == owner)
            {
                return;
            }

            if (immuneTime > 0 && newOwner == oldOwner)
            {
                return;
            }

            oldOwner = owner;
            owner = newOwner;

            Velocity = Vector3.zero;

            if (owner != null)
            {
                const float IMMUNE_TIME = 2f;
                immuneTime = IMMUNE_TIME;
            }
        }

        public bool CanChangeOwner()
        {
            return immuneTime - 1f <= 0;
        }

        private void PlaceAheadOfOwner()
        {
            var ownerVelocity = owner.Velocity;
            var ahead = ownerVelocity.normalized * 0.1f;

            var newPosition = (owner.Position + ahead);

            Position = new Vector3(newPosition.x, 0.5f, newPosition.z);
        }

        public void GetHitFromPlayer(PalinPlayer player, Vector3 destination, float speed = 10f)
        {
            if (owner != player)
            {
                return;
            }

            PlaceAheadOfOwner();
            oldOwner = owner;
            owner = null;


            var newVelocity = destination - Position;
            var newVelocityNormalized = newVelocity.normalized;

            Velocity = newVelocityNormalized * speed;



        }

        public PalinPlayer GetOwner()
        {
            return owner;
        }

        public void OnTriggerEnter(Collider other)
        {
            if (!other.CompareTag("Player"))
            {
                return;
            }

            var player = other.GetComponent<PalinPlayer>();

            if (player != null)
            {
                PalinGameService.TryToCaptureBall(player);
            }

            // lastColliderEntered = other;

        }

        public void OnTriggerStay(Collider other)
        {
            if (!other.CompareTag("Player"))
            {
                return;
            }

            var player = other.GetComponent<PalinPlayer>();

            if (player != null)
            {
                PalinGameService.TryToCaptureBall(player);
            }

            // lastColliderEntered = other;

        }

        public void Reset()
        {
            Velocity = Vector3.zero;
            Position = initialPosition;
            owner = null;
            oldOwner = null;
            immuneTime = 0f;

        }
    }
}
