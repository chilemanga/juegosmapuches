using System;
using _3_Scripts.Palin.Domain.Game;
using UnityEngine;

namespace _3_Scripts.Palin.Presentation
{
    public class PalinGoal : MonoBehaviour
    {
        [SerializeField] private Team team = Team.Undefined;

        private void Awake()
        {
            PalinGameService.RegisterGoal(team, transform.position);
        }
    }
}
