using _3_Scripts.Palin.Domain.Game;
using TMPro;
using UnityEngine;

namespace _3_Scripts.Palin.Presentation
{
    public class PalinScoreController : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI scorePlayerValue = default;
        [SerializeField] private TextMeshProUGUI scoreOpponentValue = default;
        [SerializeField] private GameObject playerMadeGoalUI = null;
        [SerializeField] private GameObject playerReceivedGoalUI = null;

        private int currPlayerScore = default;
        private int currOpponentScore = default;

        private void Awake()
        {
            PalinGameService.OnGameStart += OnGameStart;
            PalinGameService.OnScoreUpdated += UpdateScore;
        }

        private void OnDestroy()
        {
            PalinGameService.OnGameStart -= OnGameStart;
        }

        private void OnGameStart()
        {
            HideGoalUIs();
            UpdateScore((int)default, (int)default);
        }

        public void UpdateScore(int playerScore, int opponentScore)
        {
            scorePlayerValue.text = playerScore.ToString();
            scoreOpponentValue.text = opponentScore.ToString();

            playerMadeGoalUI.SetActive(currPlayerScore != playerScore);
            playerReceivedGoalUI.SetActive(currOpponentScore != opponentScore);

            currPlayerScore = playerScore;
            currOpponentScore = opponentScore;

            // After some seconds, hide the goal signs
            Invoke(nameof(HideGoalUIs), 1f);
        }

        private void HideGoalUIs()
        {
            playerMadeGoalUI.SetActive(false);
            playerReceivedGoalUI.SetActive(false);
        }

    }
}
