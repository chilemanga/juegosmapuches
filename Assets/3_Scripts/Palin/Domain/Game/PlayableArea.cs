namespace _3_Scripts.Palin.Domain.Game
{
    public enum PlayableArea
    {
        Undefined = 0,
        Left = 1,
        Middle = 2,
        Right = 3,
    }
}
