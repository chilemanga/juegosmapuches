using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using _3_Scripts.Common.Infrastructure.CoroutineRunner;
using _3_Scripts.Common.Infrastructure.Steering;
using _3_Scripts.Palin.Presentation;
using _3_Scripts.Palin.Presentation.PalinPlayers;
using UnityEngine;
using Random = UnityEngine.Random;

namespace _3_Scripts.Palin.Domain.Game
{
    public static class PalinGameService
    {
        public const string PLAYABLE_AREA_TAG = "PalinPlayableArea";

        public static event Action OnGameStart = null;
        public static event Action OnGameFinish = null;
        public static event Action<int, int> OnScoreUpdated = null;
        public static event Action OnPlayerWin = null;
        public static event Action OnPlayerLose = null;

        private static Dictionary<Team, List<PalinPlayer>> playersPerTeam = null;
        private static List<IBoid> cachedAllPlayers = null;
        private static PalinBall ball = null;

        private static int teamAScore = 0;
        private static int teamBScore = 0;
        public static bool GameRunning { get; private set; } = false;
        public static PlayableArea CurrentActiveArea { get; private set; } = PlayableArea.Undefined;

        private static Dictionary<Team, Vector3> goalZonePositionPerTeam = null;
        private static List<Vector3> fieldLimits = null;

        public static void RegisterBall(PalinBall ball)
        {
            PalinGameService.ball = ball;

            // TODO move to another place...
            Application.targetFrameRate = 30;
        }

        public static void RegisterPlayer(PalinPlayer player)
        {
            if (playersPerTeam == null)
            {
                playersPerTeam = new Dictionary<Team, List<PalinPlayer>>();
            }

            var playerTeam = player.playerTeam;

            if (!playersPerTeam.ContainsKey(playerTeam))
            {
                playersPerTeam[playerTeam] = new List<PalinPlayer>();
            }

            playersPerTeam[playerTeam].Add(player);

            if (cachedAllPlayers == null)
            {
                cachedAllPlayers = new List<IBoid>();
            }
            cachedAllPlayers.Add(player);
        }

        public static void RegisterGoal(Team team, Vector3 goalPosition)
        {
            if (goalZonePositionPerTeam == null)
            {
                goalZonePositionPerTeam = new Dictionary<Team, Vector3>();
            }


            goalZonePositionPerTeam[team] = goalPosition;
        }

        public static PalinBall GetBall()
        {
            return ball;
        }

        public static PalinPlayer GetClosestPlayerFromBall(PalinPlayer invokerPlayer, bool sameTeamOnly, bool ignorePlayer)
        {
            var closestPlayer = default(PalinPlayer);
            var closestDistance = float.PositiveInfinity;

            foreach (var kvp in playersPerTeam)
            {
                var team = kvp.Key;
                if (sameTeamOnly && team != invokerPlayer.playerTeam)
                {
                    continue;
                }

                var players = kvp.Value;
                foreach (var player in players)
                {
                    if (ignorePlayer && player == invokerPlayer)
                    {
                        continue;
                    }

                    var distance = Vector3.Distance(player.Position, ball.Position);
                    if (distance >= closestDistance)
                    {
                        continue;
                    }

                    closestDistance = distance;
                    closestPlayer = player;
                }
            }

            return closestPlayer;
        }

        public static int CountPlayersWithinRadius(Team teamToCount, Vector3 position, float radius)
        {
            if (!playersPerTeam.ContainsKey(teamToCount))
            {
                return 0;
            }

            var players = playersPerTeam[teamToCount];
            return players.Select(player => Vector3.Distance(player.Position, position)).Count(distance => distance < radius);
        }

        public static Vector3 GetOpponentGoalPosition(Team playerTeam)
        {
            var opponentGoalPosition = default(Vector3);
            foreach (var kvp in goalZonePositionPerTeam)
            {
                var team = kvp.Key;
                if (team == playerTeam)
                {
                    continue;
                }

                opponentGoalPosition = kvp.Value;
                break;

            }

            return opponentGoalPosition;
        }

        public static void TryToCaptureBall(PalinPlayer palinPlayer)
        {
            // Debug.Log($"TryToCaptureBall Player:[{palinPlayer.name}];");
            var currentBallOwner = ball.GetOwner();
            if (currentBallOwner == null)
            {
                ball.SetOwner(palinPlayer);
                return;
            }

            if (currentBallOwner == palinPlayer)
            {
                return;
            }

            if (currentBallOwner.playerTeam == palinPlayer.playerTeam)
            {
                return;
            }

            // check who owns the ball....
            var currentOwnerChancesToKeep = currentBallOwner.GetOwnershipChances();
            var ownerKeepScore = Random.value * currentOwnerChancesToKeep;

            var challengerChancesToCapture = currentBallOwner.GetOwnershipChances();
            var challengerCaptureScore = Random.value * challengerChancesToCapture;

            if (challengerCaptureScore > ownerKeepScore)
            {
                ball.SetOwner(palinPlayer);
            }
        }

        public static bool CheckIfIsControlledByUser(PalinPlayer palinPlayer)
        {
            var userTeam = Team.TeamA;
            if (palinPlayer.playerTeam != userTeam)
            {
                return false;
            }

            var currentBallOwner = ball.GetOwner();
            if (palinPlayer == currentBallOwner)
            {
                return true;
            }

            // there is a ball owner in userTeam, but is not this specific palinPlayer...
            if (currentBallOwner != null && currentBallOwner.playerTeam == userTeam && palinPlayer != currentBallOwner)
            {
                return false;
            }

            // TODO; else... player controlled by user will be the closest to ball..
            // we can have an independent per frame method to have a distance ranking and choose from there... [to avoid the same foreach for each player]

            var closest = GetClosestPlayerFromBall(palinPlayer, true, false);
            return palinPlayer == closest;
        }

        public static List<IBoid> GetAllPlayers()
        {
            return cachedAllPlayers;
        }

        public static Team GetOpponentTeam(PalinPlayer player)
        {
            return player.playerTeam == Team.TeamA ? Team.TeamB : Team.TeamA;
        }

        public static List<PalinPlayer> GetPlayersFromTeam(Team playerTeam)
        {
            return playersPerTeam[playerTeam];
        }

        public static void StartGame()
        {
            teamAScore = 0;
            teamBScore = 0;
            CurrentActiveArea = PlayableArea.Undefined;

            OnGameStart?.Invoke();
            GameRunning = true;
        }

        private static void RestartGameAfterScored()
        {
            GameRunning = false;
            ScoreUpdated();

            ball.Reset();

            foreach (var playerTeam in playersPerTeam)
            {
                var players = playerTeam.Value;
                foreach (var player in players)
                {
                    player.Reset();
                }
            }

            const int DIFF_TO_WIN = 4;

            var playerDiff = teamAScore - teamBScore;
            var opponentDiff = -playerDiff;
            if (playerDiff >= DIFF_TO_WIN)
            {
                // player win
                OnPlayerWin?.Invoke();
            }
            else if (opponentDiff >= DIFF_TO_WIN)
            {
                // player lose
                OnPlayerLose?.Invoke();
            }
            else
            {
                StaticCoroutineRunner.Instance.Run(DelayedStart(2f));
            }
        }

        private static IEnumerator DelayedStart(float delay)
        {
            yield return new WaitForSeconds(delay);

            GameRunning = true;
        }

        public static void ScoreUpdated()
        {
            OnScoreUpdated?.Invoke(teamAScore, teamBScore);
        }

        public static void ReportBallPosition(IBoid ballBoid)
        {
            var ballPosX = ballBoid.Position.z;

            if (ballPosX < goalZonePositionPerTeam[Team.TeamA].z)
            {
                //Score B
                Debug.Log("score B ++");
                teamBScore++;
                RestartGameAfterScored();
            }
            else if (goalZonePositionPerTeam[Team.TeamB].z < ballPosX)
            {
                // score A
                Debug.Log("score A ++");
                teamAScore++;
                RestartGameAfterScored();
            }
            else if (OutOfLimits(ballBoid))
            {
                RestartGameAfterScored();
            }

        }

        public static void RegisterFieldLimit(Vector3 corner)
        {
            if (fieldLimits == null)
            {
                fieldLimits = new List<Vector3>();
            }

            fieldLimits.Add(corner);
        }

        private static bool OutOfLimits(IBoid boid)
        {
            var toTheRightOfLimitCounter = fieldLimits.Sum(fieldLimit => fieldLimit.x < boid.Position.x ? 1 : -1);
            return Math.Abs(toTheRightOfLimitCounter) == fieldLimits.Count;
        }

        public static void NotifyActiveArea(PlayableArea playableArea)
        {
            CurrentActiveArea = playableArea;
        }

        public static Vector3 GetClosestBorderPosition(Vector3 position)
        {
            var closestDistance = Mathf.Infinity;
            var closestLimit = Vector3.zero;

            foreach (var fieldLimit in fieldLimits)
            {
                var distance = Mathf.Abs(position.x - fieldLimit.x);
                if (distance < closestDistance)
                {
                    closestDistance = distance;
                    closestLimit = fieldLimit;
                }
            }

            return new Vector3(closestLimit.x, position.y, position.z);
        }
    }
}
