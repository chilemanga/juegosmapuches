namespace _3_Scripts.Palin.Domain.Game
{
    public enum PlayerType
    {
        Undefined = 0,
        Attacker = 1,
        Defender = 2,
        Middle = 3,
    }
}
