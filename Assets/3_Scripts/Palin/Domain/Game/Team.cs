namespace _3_Scripts.Palin.Domain.Game
{
    public enum Team
    {
        Undefined = 0,
        TeamA = 1,
        TeamB = 2,
    }
}
