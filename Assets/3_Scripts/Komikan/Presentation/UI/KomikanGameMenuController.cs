using System;
using System.Collections;
using _3_Scripts.Komikan.Domain.Board;
using _3_Scripts.Komikan.Domain.GameLogic;
using _3_Scripts.Komikan.Presentation.Cameras;
using UnityEngine;

namespace _3_Scripts.Komikan.Presentation.UI
{
    public class KomikanGameMenuController : MonoBehaviour
    {
        [SerializeField]
        private GameObject mainUIMenu = null;

        [SerializeField]
        private GameObject inGameUINode = null;

        [SerializeField]
        private GameObject playersTurnUINode = null;

        [SerializeField]
        private GameObject playerWon = null;

        [SerializeField]
        private GameObject playerLoose = null;

        [SerializeField]
        private DogEatenCounterController dogEatenCounterController = null;

        [SerializeField]
        private KomikanVirtualCameraController virtualCameraController = null;

        private void Awake()
        {
            BoardService.OnDogEatenEvent += OnDogEaten;
            GameFlowService.OnPlayersTurn += OnPlayersTurn;
            GameFlowService.OnPlayerWon += OnPlayerWon;
            GameFlowService.OnPlayerLoose += OnPlayerLoose;
        }

        private void OnPlayersTurn()
        {
            IEnumerator ShowYourTurnUI()
            {
                playersTurnUINode.SetActive(true);

                yield return new WaitForSeconds(1f);

                playersTurnUINode.SetActive(false);
            }

            StartCoroutine(ShowYourTurnUI());
        }

        private void OnDestroy()
        {
            BoardService.OnDogEatenEvent -= OnDogEaten;
            GameFlowService.OnPlayersTurn -= OnPlayersTurn;
            GameFlowService.OnPlayerWon -= OnPlayerWon;
            GameFlowService.OnPlayerLoose -= OnPlayerLoose;
        }

        public void StarAsDogs()
        {
            StartGame(BoardService.GameType.Dogs);
        }

        public void StartAsPuma()
        {
            StartGame(BoardService.GameType.Puma);
        }

        private void StartGame(BoardService.GameType gameType)
        {
            dogEatenCounterController.RestartCounter();
            virtualCameraController.StartGame(gameType);

            playersTurnUINode.SetActive(false);
            mainUIMenu.SetActive(false);
            inGameUINode.SetActive(true);

            GameFlowService.StartGame(gameType);
        }

        public void BackToMenu()
        {
            playersTurnUINode.SetActive(false);
            playerWon.SetActive(false);
            playerLoose.SetActive(false);
            inGameUINode.SetActive(false);
            mainUIMenu.SetActive(true);
        }

        private void OnDogEaten(int amount)
        {
            dogEatenCounterController.SetAmount(amount);
        }

        private void OnPlayerWon()
        {
            playersTurnUINode.SetActive(false);
            virtualCameraController.FinishGame();
            playerWon.SetActive(true);
        }

        private void OnPlayerLoose()
        {
            playersTurnUINode.SetActive(false);
            virtualCameraController.FinishGame();
            playerLoose.SetActive(true);
        }

    }
}
