﻿using TMPro;
using UnityEngine;

namespace _3_Scripts.Komikan.Presentation.UI
{
    public class DogEatenCounterController : MonoBehaviour
    {
        private const string ZERO_TEXT = "00";
        private const string TEXT_FORMAT = "{0:00}";

        [SerializeField]
        private TextMeshProUGUI counterText = null;


        public void RestartCounter()
        {
            counterText.text = ZERO_TEXT;
        }

        public void SetAmount(int newAmount)
        {
            counterText.text = string.Format(TEXT_FORMAT, newAmount) ;
        }



    }
}
