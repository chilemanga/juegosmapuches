using System;
using _3_Scripts.Komikan.Domain.Board;
using Cinemachine;
using UnityEngine;

namespace _3_Scripts.Komikan.Presentation.Cameras
{
    public class KomikanVirtualCameraController : MonoBehaviour
    {
        [SerializeField]
        private GameObject pumaStaticVirtualCamera = null;

        [SerializeField]
        private GameObject dogsStaticVirtualCamera = null;

        [SerializeField]
        private GameObject pumaPlayingVirtualCamera = null;

        [SerializeField]
        private GameObject dogsPlayingVirtualCamera = null;

        [SerializeField]
        private CinemachineVirtualCamera dogsPlayingCinemachineCamera = null;

        private void Awake()
        {
            Reset();
        }

        private void Reset()
        {
            dogsStaticVirtualCamera.SetActive(false);
            pumaStaticVirtualCamera.SetActive(false);
            pumaPlayingVirtualCamera.SetActive(false);
            dogsPlayingVirtualCamera.SetActive(false);
        }

        public void StartGame(BoardService.GameType gameType)
        {
            dogsStaticVirtualCamera.SetActive(BoardService.GameType.Dogs == gameType);
            dogsPlayingVirtualCamera.SetActive(BoardService.GameType.Dogs == gameType);

            pumaStaticVirtualCamera.SetActive(BoardService.GameType.Puma == gameType);
            pumaPlayingVirtualCamera.SetActive(BoardService.GameType.Puma == gameType);
        }

        public void FinishGame()
        {
            dogsPlayingVirtualCamera.SetActive(false);
            pumaPlayingVirtualCamera.SetActive(false);
        }

        public void SetDogsPlayingCameraTarget(Transform newTarget)
        {
            dogsPlayingCinemachineCamera.Follow = newTarget;
            dogsPlayingVirtualCamera.SetActive(true);
        }

        public void UnsetDogsPlayingCameraTarget()
        {
            dogsPlayingCinemachineCamera.Follow = null;
            dogsPlayingVirtualCamera.SetActive(false);
        }
    }
}
