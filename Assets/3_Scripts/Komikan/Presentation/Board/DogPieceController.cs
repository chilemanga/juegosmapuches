using System;
using _3_Scripts.Komikan.Domain.Board;

namespace KurGames.Mapuches.Komikan.Presentation.Board
{
    public class DogPieceController : AbstractPiece, IPiece
    {
        protected override void Register()
        {
            BoardService.RegisterDog(this);
        }


    }
}
