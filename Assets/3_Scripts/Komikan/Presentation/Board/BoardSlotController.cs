using System;
using System.Collections.Generic;
using _3_Scripts.Komikan.Domain.Board;
using _3_Scripts.Komikan.Presentation.Cameras;
using UnityEngine;
using UnityEngine.EventSystems;

namespace KurGames.Mapuches.Komikan.Presentation.Board
{
    public class BoardSlotController : MonoBehaviour, IBoardSlot, IPointerClickHandler
    {
        [Header(" Komikan Virtual Camera Controller")]
        [SerializeField]
        private KomikanVirtualCameraController komikanVirtualCameraController = null;

        [SerializeField]
        private BoardSlotStatusController boardSlotStatusController = null;

        [Header(" Neighbors on XY-axys")]
        [SerializeField]
        private BoardSlotController upNeighbor = null;
        [SerializeField]
        private BoardSlotController rightNeighbor = null;
        [SerializeField]
        private BoardSlotController downNeighbor = null;
        [SerializeField]
        private BoardSlotController leftNeighbor = null;


        [Space(10)]
        [Header(" Neighbors on Diagonals")]
        [SerializeField]
        private BoardSlotController upRightNeighbor = null;
        [SerializeField]
        private BoardSlotController downRightNeighbor = null;
        [SerializeField]
        private BoardSlotController downLeftNeighbor = null;
        [SerializeField]
        private BoardSlotController upLeftNeighbor = null;

        public IBoardSlot UpNeighbor => upNeighbor;
        public IBoardSlot DownNeighbor => downNeighbor;
        public IBoardSlot LeftNeighbor => leftNeighbor;
        public IBoardSlot RightNeighbor => rightNeighbor;
        public IBoardSlot UpLeftNeighbor => upLeftNeighbor;
        public IBoardSlot UpRightNeighbor => upRightNeighbor;
        public IBoardSlot DownLeftNeighbor => downLeftNeighbor;
        public IBoardSlot DownRightNeighbor => downRightNeighbor;

        public bool ContainsDog
        {
            get
            {
                var piece = GetComponentInChildren<AbstractPiece>();
                return piece != null && piece.IsDog();
            }
        }

        public bool ContainsPuma
        {
            get
            {
                var piece = GetComponentInChildren<AbstractPiece>();
                return piece != null && piece.IsPuma();
            }
        }

        private List<IBoardSlot> neighbors = new List<IBoardSlot>();

        private void Awake()
        {
            if (upNeighbor != null)
            {
                neighbors.Add(upNeighbor);
            }
            if (downNeighbor != null)
            {
                neighbors.Add(downNeighbor);
            }
            if (leftNeighbor != null)
            {
                neighbors.Add(leftNeighbor);
            }
            if (rightNeighbor != null)
            {
                neighbors.Add(rightNeighbor);
            }

            if (upRightNeighbor != null)
            {
                neighbors.Add(upRightNeighbor);
            }
            if (downRightNeighbor != null)
            {
                neighbors.Add(downRightNeighbor);
            }
            if (downLeftNeighbor != null)
            {
                neighbors.Add(downLeftNeighbor);
            }
            if (upLeftNeighbor != null)
            {
                neighbors.Add(upLeftNeighbor);
            }
        }

        private void Start()
        {
            BoardService.Register(this);
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            Debug.Log("Click on board slot!");
            BoardService.Select(this, BoardService.HighlightMode.All);
        }

        public AbstractPiece GetPieceOnSlot()
        {
            return GetComponentInChildren<AbstractPiece>();
        }

        public bool SetPiece(AbstractPiece piece)
        {
            if (!IsSlotEmpty())
            {
                return false;
            }

            var pieceObject = piece.gameObject;
            pieceObject.transform.SetParent(transform, false);

            return true;
        }

        public bool IsSlotEmpty()
        {
            var currentSlotPiece = GetComponentInChildren<AbstractPiece>();

            if (currentSlotPiece == null)
            {
                return true;
            }

            Debug.LogWarning($"Slot already has a piece: [{currentSlotPiece.name}] ");
            return false;
        }

        public bool CleanSlot()
        {
            throw new System.NotImplementedException();
        }

        public List<IBoardSlot> GetValidNeighbors()
        {
            return neighbors;
        }

        public void EnableHighlight()
        {
            // But only show if is available...
            var isSlotEmpty = IsSlotEmpty();
            boardSlotStatusController.EnablePossibleHighlight(isSlotEmpty);
        }

        public void DisableHighlight()
        {
            boardSlotStatusController.EnablePossibleHighlight(false);
        }

        public void DisableHighlightOnNeighbors()
        {
            neighbors.ForEach(n => n.DisableHighlight());
        }

        public void MarkSlotAsSelected()
        {
            // TODO SM; report to some element that gets info and decide to make specific-camera follow this piece
            if (IsSlotEmpty())
            {
                return;
            }

            var piece = GetPieceOnSlot();
            if (piece == null || piece.IsPuma())
            {
                return;
            }

            komikanVirtualCameraController.SetDogsPlayingCameraTarget(this.transform);
        }

        public void UnselectSlot()
        {
            komikanVirtualCameraController.UnsetDogsPlayingCameraTarget();
        }
    }
}
