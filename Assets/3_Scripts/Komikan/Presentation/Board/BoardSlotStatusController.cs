using System;
using UnityEngine;

namespace KurGames.Mapuches.Komikan.Presentation.Board
{
    public class BoardSlotStatusController : MonoBehaviour
    {
        [SerializeField]
        private GameObject possible = default;

        private void Awake()
        {
            EnablePossibleHighlight(false);
        }

        public void EnablePossibleHighlight(bool active)
        {
            possible.SetActive(active);
        }
    }
}
