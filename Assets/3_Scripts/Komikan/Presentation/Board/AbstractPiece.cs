﻿using System;
using System.Threading;
using _3_Scripts.Komikan.Domain.Board;
using UnityEngine;
using UnityEngine.EventSystems;

namespace KurGames.Mapuches.Komikan.Presentation.Board
{
    public abstract class AbstractPiece : MonoBehaviour
    {
        [SerializeField]
        protected PieceType pieceType = PieceType.Undefined;

        [SerializeField]
        protected GameObject selectedStatus = null;

        [SerializeField]
        protected GameObject canBeSelectedStatus = null;

        private Transform originalParent = null;
        
        private void Awake()
        {
            SaveOriginalParent();
            DeactivateStatusObjects();
            Register();
        }

        private void SaveOriginalParent()
        {
            originalParent = transform.parent;
        }

        private void ResetToOriginalParent()
        {
            transform.SetParent(originalParent, false);
        }

        protected abstract void Register();

        public void TurnStarted()
        {
            canBeSelectedStatus.SetActive(true);
            selectedStatus.SetActive(false);
        }

        public bool Select()
        {
            var validTurn = BoardService.IsOnPieceTypeTurn(pieceType);

            if (!validTurn)
            {
                return false;
            }

            canBeSelectedStatus.SetActive(false);
            selectedStatus.SetActive(true);

            return true;
        }

        public void TurnEnded()
        {
            DeactivateStatusObjects();
        }

        private void DeactivateStatusObjects()
        {
            canBeSelectedStatus.SetActive(false);
            selectedStatus.SetActive(false);
        }

        public bool IsDog()
        {
            return pieceType == PieceType.Dog;
        }

        public bool IsPuma()
        {
            return pieceType == PieceType.Puma;
        }

        public PieceType GetPieceType()
        {
            return pieceType;
        }

        public void Remove()
        {
            this.gameObject.SetActive(false);
        }

        public void ReSpawn()
        {
            ResetToOriginalParent();
            this.gameObject.SetActive(true);
        }
    }

}


