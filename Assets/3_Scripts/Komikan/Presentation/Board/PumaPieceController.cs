using _3_Scripts.Komikan.Domain.Board;

namespace KurGames.Mapuches.Komikan.Presentation.Board
{
    public class PumaPieceController : AbstractPiece, IPiece
    {
        protected override void Register()
        {
            BoardService.RegisterPuma(this);
        }

    }
}
