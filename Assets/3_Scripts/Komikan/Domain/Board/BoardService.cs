using System;
using System.Collections.Generic;
using System.Linq;
using _3_Scripts.Komikan.Domain.GameLogic;
using _3_Scripts.Komikan.Domain.IA;
using KurGames.Mapuches.Komikan.Presentation.Board;
using UnityEngine;

namespace _3_Scripts.Komikan.Domain.Board
{
    public static class BoardService
    {
        private const int EATEN_DOGS_REQUIRED_TO_WIN = 6;

        private static readonly List<IBoardSlot> slots = new List<IBoardSlot>();
        private static readonly List<IPiece> allDogs = new List<IPiece>();
        private static readonly List<IPiece> allPumas = new List<IPiece>();

        private static PumaIA pumaIA = null;
        private static DogsIA dogIA = null;

        private static IBoardSlot currentSelectedSlot = null;
        private static PieceType currentPieceTurn = PieceType.Undefined;
        private static int dogsEaten = default;
        private static bool pumaHasBeenEating = false;

        public static event Action<int> OnDogEatenEvent = delegate {};

        public enum GameType
        {
            Undefined = 0,
            Dogs,
            Puma,
        }

        public enum HighlightMode
        {
            None = 0,
            All = 1,
            OnlyEdible = 2,
        }


        public static void StartGame(GameType gameType)
        {
            pumaIA = gameType == GameType.Puma ? null : new PumaIA();
            dogIA = gameType == GameType.Dogs ? null : new DogsIA();
        }

        public static bool IsDogAIPlaying()
        {
            return dogIA != null;
        }

        public static bool IsPumaAIPlaying()
        {
            return pumaIA != null;
        }

        public static void RegisterDog(IPiece dog)
        {
            allDogs.Add(dog);
        }

        public static void RegisterPuma(IPiece puma)
        {
            allPumas.Add(puma);
        }

        public static void Register(IBoardSlot boardSlot)
        {
            slots.Add(boardSlot);
        }

        public static void Select(IBoardSlot boardSlot, HighlightMode highlightMode)
        {
            var piece = boardSlot.GetPieceOnSlot();
            // Select piece to move...
            if (currentSelectedSlot == null)
            {
                currentSelectedSlot = SelectionMove(boardSlot, piece, highlightMode);
                if (currentSelectedSlot == null)
                {
                    return;
                }

                currentSelectedSlot.MarkSlotAsSelected();
            }
            else // selecting new slot to move...
            {
                DestinationMove( currentSelectedSlot,boardSlot);
            }
        }

        public static void Cleanup()
        {
            currentPieceTurn = PieceType.Undefined;
            dogsEaten = 0;

            allPumas.ForEach(p => p.ReSpawn());
            allDogs.ForEach(p => p.ReSpawn());
        }

        public static void StartDogsTurn()
        {
            Debug.Log("Start dogs turn!");
            currentPieceTurn = PieceType.Dog;
            pumaHasBeenEating = false;

            allDogs.ForEach(d => d.TurnStarted());

            if (dogIA == null)
            {
                return;
            }

            var selectedDogSlot = dogIA.GetOrigin(slots);
            Select(selectedDogSlot, HighlightMode.None);

            var destinationSlot = dogIA.GetDestination(selectedDogSlot);
            Select(destinationSlot, HighlightMode.None);
        }

        public static void StartPumaTurn()
        {
            Debug.Log("Start pumas turn!");
            currentPieceTurn = PieceType.Puma;
            pumaHasBeenEating = false;

            allPumas.ForEach(d => d.TurnStarted());

            if (pumaIA == null)
            {
                return;
            }

            var pumaSlot = pumaIA.GetOrigin(slots);
            Select(pumaSlot, HighlightMode.None);

            var pumaEdibleOptions = GetPumaEdibleOptions(pumaSlot);
            var edibleMovesWereFound = pumaEdibleOptions.Any();

            while (pumaEdibleOptions.Any())
            {
                var destinationEdibleSlot = pumaIA.GetDestinationEdible(pumaEdibleOptions);
                Select(destinationEdibleSlot, HighlightMode.None);
                pumaHasBeenEating = true;
                pumaEdibleOptions = GetPumaEdibleOptions(destinationEdibleSlot);
            }

            if (edibleMovesWereFound)
            {
                return;
            }

            var pumaNeighbor = pumaIA.GetDestination(pumaSlot);
            Select(pumaNeighbor, HighlightMode.None);
        }

        public static bool IsOnPieceTypeTurn(PieceType pieceType)
        {
            return currentPieceTurn == pieceType;
        }

        public static bool DogsWon()
        {
            var pumaSlot = slots.First(s => s.ContainsPuma);

            var pumaDisplacementMoves = pumaSlot.GetValidNeighbors();

            var pumaEdibleMoveSlots = GetPumaEdibleOptions(pumaSlot);
            var pumaEdibleMoves = pumaEdibleMoveSlots.Keys;

            return !pumaDisplacementMoves.Union(pumaEdibleMoves).Any();
        }

        public static bool PumaWon()
        {
            return dogsEaten >= EATEN_DOGS_REQUIRED_TO_WIN;
        }

        private static IBoardSlot SelectionMove(IBoardSlot boardSlot, AbstractPiece piece, HighlightMode highlightMode)
        {
            if (piece == null)
            {
                Debug.Log("There is no piece to be selected!");
                return null;
            }

            var isRightTurn = IsOnPieceTypeTurn(piece.GetPieceType());
            if (!isRightTurn)
            {
                Debug.Log("The is not the turn for the team of selected piece!");
                return null;
            }

            // Highlight possible moves
            var validNeighborMoves = boardSlot.GetValidNeighbors();
            if (highlightMode == HighlightMode.All)
            {
                validNeighborMoves.ForEach(n => n.EnableHighlight());
            }

            if (piece.IsPuma() && highlightMode == HighlightMode.All || highlightMode == HighlightMode.OnlyEdible)
            {
                // special case puma
                var pumaEdibleMoveSlots = GetPumaEdibleOptions(boardSlot);
                foreach (var pumaEdibleMoveSlot in pumaEdibleMoveSlots)
                {
                    var slot = pumaEdibleMoveSlot.Key;
                    slot.EnableHighlight();
                }
            }

            piece.Select();

            return boardSlot;
        }

        private static void DestinationMove(IBoardSlot originSlot, IBoardSlot destinationSlot)
        {
            var currentSelectedPiece = originSlot.GetPieceOnSlot();

            var isRightTurn = IsOnPieceTypeTurn(currentPieceTurn);
            if (!isRightTurn)
            {
                Debug.Log("The is not the turn for the team of selected piece!");
                return;
            }

            var emptySlot = destinationSlot.IsSlotEmpty();

            // if the destination slot is not empty, ignore, unless is dogs' turn and another dog is selected...
            if (!emptySlot)
            {
                if (!currentSelectedPiece.IsDog())
                {
                    return;
                }

                if (!destinationSlot.ContainsDog)
                {
                    return;
                }

                UnselectSelection();
                Select(destinationSlot, HighlightMode.All);

                return;
            }

            var validNeighborMoves = originSlot.GetValidNeighbors();
            var validPumaEdibleMoves = GetPumaEdibleOptions(originSlot);
            var edibleMoves = validPumaEdibleMoves.Keys;
            if (!validNeighborMoves.Contains(destinationSlot) && !edibleMoves.Contains(destinationSlot))
            {
                Debug.Log("Illegal move!");

                return;
            }

            // if Puma has been eating, but current destination slot is not another edible move,
            // instead of moving (it cannot) just end the puma turn...
            var isAPumaEdibleMove = edibleMoves.Contains(destinationSlot);
            if (pumaHasBeenEating && !isAPumaEdibleMove)
            {
                Debug.Log("Finishing puma_specific turn... [able to keep eating, but option discarded]");
                EndsCurrentTurn();
                return;
            }

            var success = destinationSlot.SetPiece(currentSelectedPiece);
            if (!success)
            {
                Debug.LogError("Set selected piece on destination slot was not possible!!");

                return;
            }

            if (isAPumaEdibleMove)
            {
                var attackedDog = validPumaEdibleMoves[destinationSlot];
                attackedDog.Remove();
                dogsEaten++;
                OnDogEatenEvent?.Invoke(dogsEaten);

                var updatedPossiblePumaMoves = GetPumaEdibleOptions(destinationSlot);
                if (updatedPossiblePumaMoves.Any())
                {

                    UnselectSelection();
                    pumaHasBeenEating = true;
                    Select(destinationSlot, HighlightMode.OnlyEdible);

                    return;
                }
            }

            EndsCurrentTurn();
        }

        private static Dictionary<IBoardSlot, AbstractPiece> GetPumaEdibleOptions(IBoardSlot boardSlot)
        {
            var edibleMoves = new Dictionary<IBoardSlot, AbstractPiece>();

            var upNeighbor = boardSlot.UpNeighbor;
            if (upNeighbor != null && !upNeighbor.IsSlotEmpty())
            {
                var nextInLineUp = upNeighbor.UpNeighbor;
                if (nextInLineUp != null && nextInLineUp.IsSlotEmpty())
                {
                    edibleMoves[nextInLineUp] = upNeighbor.GetPieceOnSlot();
                }
            }

            var downNeighbor = boardSlot.DownNeighbor;
            if (downNeighbor != null && !downNeighbor.IsSlotEmpty())
            {
                var nextInLineUp = downNeighbor.DownNeighbor;
                if (nextInLineUp != null && nextInLineUp.IsSlotEmpty())
                {
                    edibleMoves[nextInLineUp] = downNeighbor.GetPieceOnSlot();
                }
            }

            var leftNeighbor = boardSlot.LeftNeighbor;
            if (leftNeighbor != null && !leftNeighbor.IsSlotEmpty())
            {
                var nextInLineUp = leftNeighbor.LeftNeighbor;
                if (nextInLineUp != null && nextInLineUp.IsSlotEmpty())
                {
                    edibleMoves[nextInLineUp] = leftNeighbor.GetPieceOnSlot();
                }
            }

            var rightNeighbor = boardSlot.RightNeighbor;
            if (rightNeighbor != null && !rightNeighbor.IsSlotEmpty())
            {
                var nextInLineUp = rightNeighbor.RightNeighbor;
                if (nextInLineUp != null && nextInLineUp.IsSlotEmpty())
                {
                    edibleMoves[nextInLineUp] = rightNeighbor.GetPieceOnSlot();
                }
            }

            // check diagonals...
            var upRightNeighbor = boardSlot.UpRightNeighbor;
            if (upRightNeighbor != null && !upRightNeighbor.IsSlotEmpty())
            {
                var nextInLineUp = upRightNeighbor.UpRightNeighbor;
                if (nextInLineUp != null && nextInLineUp.IsSlotEmpty())
                {
                    edibleMoves[nextInLineUp] = upRightNeighbor.GetPieceOnSlot();
                }
            }

            var downRightNeighbor = boardSlot.DownRightNeighbor;
            if (downRightNeighbor != null && !downRightNeighbor.IsSlotEmpty())
            {
                var nextInLineUp = downRightNeighbor.DownRightNeighbor;
                if (nextInLineUp != null && nextInLineUp.IsSlotEmpty())
                {
                    edibleMoves[nextInLineUp] = downRightNeighbor.GetPieceOnSlot();
                }
            }

            var upLeftNeighbor = boardSlot.UpLeftNeighbor;
            if (upLeftNeighbor != null && !upLeftNeighbor.IsSlotEmpty())
            {
                var nextInLineUp = upLeftNeighbor.UpLeftNeighbor;
                if (nextInLineUp != null && nextInLineUp.IsSlotEmpty())
                {
                    edibleMoves[nextInLineUp] = upLeftNeighbor.GetPieceOnSlot();
                }
            }

            var downLeftNeighbor = boardSlot.DownLeftNeighbor;
            if (downLeftNeighbor != null && !downLeftNeighbor.IsSlotEmpty())
            {
                var nextInLineUp = downLeftNeighbor.DownLeftNeighbor;
                if (nextInLineUp != null && nextInLineUp.IsSlotEmpty())
                {
                    edibleMoves[nextInLineUp] = downLeftNeighbor.GetPieceOnSlot();
                }
            }

            return edibleMoves;
        }

        private static void EndsCurrentTurn()
        {
            allDogs.ForEach(d => d.TurnEnded());
            allPumas.ForEach(d => d.TurnEnded());
            pumaHasBeenEating = false;

            UnselectSelection();

            currentPieceTurn = PieceType.Undefined;

            GameFlowService.FinishCurrentTurn();
        }

        private static void UnselectSelection()
        {
            var neighbors = currentSelectedSlot.GetValidNeighbors();
            neighbors.ForEach(n => n.DisableHighlight());
            neighbors.ForEach(n => n.DisableHighlightOnNeighbors());

            currentSelectedSlot.UnselectSlot();
            currentSelectedSlot = null;

        }

    }
}
