namespace _3_Scripts.Komikan.Domain.Board
{
    public enum PieceType
    {
        Undefined = 0,
        Puma = 1,
        Dog = 2,
    }
}
