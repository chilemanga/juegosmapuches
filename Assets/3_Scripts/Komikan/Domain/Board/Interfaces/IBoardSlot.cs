using System.Collections.Generic;
using KurGames.Mapuches.Komikan.Presentation.Board;
using UnityEngine;

namespace _3_Scripts.Komikan.Domain.Board
{
    public interface IBoardSlot
    {
        IBoardSlot UpNeighbor { get; }
        IBoardSlot DownNeighbor { get; }
        IBoardSlot LeftNeighbor { get; }
        IBoardSlot RightNeighbor { get; }

        IBoardSlot UpLeftNeighbor { get; }
        IBoardSlot UpRightNeighbor { get; }

        IBoardSlot DownLeftNeighbor { get; }
        IBoardSlot DownRightNeighbor { get; }

        bool ContainsDog { get; }
        bool ContainsPuma { get; }

        bool IsSlotEmpty();

        AbstractPiece GetPieceOnSlot();

        bool SetPiece(AbstractPiece piece);
        bool CleanSlot();

        List<IBoardSlot> GetValidNeighbors();

        void EnableHighlight();
        void DisableHighlight();

        void DisableHighlightOnNeighbors();

        void MarkSlotAsSelected();
        void UnselectSlot();

    }
}
