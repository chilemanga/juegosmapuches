namespace _3_Scripts.Komikan.Domain.Board
{
    public interface IPiece
    {

        void TurnStarted();
        void TurnEnded();
        void ReSpawn();
    }
}
