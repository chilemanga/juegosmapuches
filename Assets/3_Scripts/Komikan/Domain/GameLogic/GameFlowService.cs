using System;
using System.Collections;
using _3_Scripts.Common.Infrastructure.CoroutineRunner;
using _3_Scripts.Komikan.Domain.Board;
using UnityEngine;

namespace _3_Scripts.Komikan.Domain.GameLogic
{
    public static class GameFlowService
    {
        private const float IA_THINKING_DELAY = 2f;

        private static bool waitingForTurnToFinish = false;

        private static StaticCoroutineRunner staticCoroutineRunner = default;
        public static event Action OnPlayersTurn = delegate {};
        public static event Action OnPlayerWon = delegate {};
        public static event Action OnPlayerLoose = delegate {};

        private static event Action OnPumaWon = delegate {};
        private static event Action OnDogsWon = delegate {};


        public static void StartGame(BoardService.GameType gameType)
        {
            if (gameType == BoardService.GameType.Undefined)
            {
                Debug.LogError("GameType is not defined! aborting...");
                return;
            }

            OnDogsWon = gameType == BoardService.GameType.Dogs ? OnPlayerWon : OnPlayerLoose;
            OnPumaWon = gameType == BoardService.GameType.Dogs ? OnPlayerLoose : OnPlayerWon;

            BoardService.StartGame(gameType);
            StaticCoroutineRunner.Instance.Run(GameLoop());
        }

        public static void FinishCurrentTurn()
        {
            waitingForTurnToFinish = false;
        }

        private static IEnumerator GameLoop()
        {
            Debug.Log("Komikan; Starting game loop");
            BoardService.Cleanup();

            yield return null;

            while (true)
            {
                Debug.Log("Komikan; Starting next loop");

                // Dogs Turn - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                waitingForTurnToFinish = true;

                // Extra delay time if AI is playing...
                if (BoardService.IsDogAIPlaying())
                {
                    yield return new WaitForSeconds(IA_THINKING_DELAY);
                }
                else
                {
                    OnPlayersTurn?.Invoke();
                }

                BoardService.StartDogsTurn();
                yield return new WaitWhile(() => waitingForTurnToFinish);

                if (WinConditionsAchieved())
                {
                    break;
                }

                yield return new WaitForSeconds(0.1f);

                // Pumas Turn - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                waitingForTurnToFinish = true;
                // Extra delay time if AI is playing...
                if (BoardService.IsPumaAIPlaying())
                {
                    yield return new WaitForSeconds(IA_THINKING_DELAY);
                }
                else
                {
                    OnPlayersTurn?.Invoke();
                }

                BoardService.StartPumaTurn();
                yield return new WaitWhile(() => waitingForTurnToFinish);

                if (WinConditionsAchieved())
                {
                    break;
                }

                yield return new WaitForSeconds(0.1f);
                //  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                Debug.Log("Komikan; Starting finishing loop");
            }

        }

        private static bool WinConditionsAchieved()
        {
            if (BoardService.DogsWon())
            {
                Debug.Log("Dogs won!");
                OnDogsWon?.Invoke();
                return true;
            }

            if (BoardService.PumaWon())
            {
                Debug.Log("Puma won!");
                OnPumaWon?.Invoke();
                return true;
            }

            return false;
        }
    }
}
