using System.Collections.Generic;
using System.Linq;
using _3_Scripts.Komikan.Domain.Board;
using KurGames.Mapuches.Komikan.Presentation.Board;
using UnityEngine;

namespace _3_Scripts.Komikan.Domain.IA
{
    public class PumaIA
    {
        public IBoardSlot GetOrigin(List<IBoardSlot> slots)
        {
            var pumaSlot = slots.Single(s => s.ContainsPuma);
            return pumaSlot;
        }

        public IBoardSlot GetDestination(IBoardSlot pumaSlot)
        {
            var pumaNeighbors = pumaSlot.GetValidNeighbors();
            var pumaPossibleNeighbors = pumaNeighbors.Where(n => n.IsSlotEmpty()).ToArray();

            var randomSkip = Random.Range(0, pumaPossibleNeighbors.Count());
            var pumaNeighbor = pumaPossibleNeighbors.Skip(randomSkip).First();

            return pumaNeighbor;
        }

        public IBoardSlot GetDestinationEdible(Dictionary<IBoardSlot, AbstractPiece> pumaEdibleOptions)
        {
            if (!pumaEdibleOptions.Any())
            {
                return null;
            }

            var moveData = pumaEdibleOptions.First();
            var moveSLot = moveData.Key;

            return moveSLot;
        }
    }
}
