using System.Collections.Generic;
using System.Linq;
using _3_Scripts.Komikan.Domain.Board;
using UnityEngine;

namespace _3_Scripts.Komikan.Domain.IA
{
    public class DogsIA
    {
        public IBoardSlot GetOrigin(List<IBoardSlot> slots)
        {
            var dogSlots = slots.Where(s => s.ContainsDog);

            var validDogSlots = dogSlots.Where(slot => slot.GetValidNeighbors().Any(n => n.IsSlotEmpty()));

            var randomSkip = Random.Range(0, validDogSlots.Count());
            var selectedDogSlot = validDogSlots.Skip(randomSkip).First();

            return selectedDogSlot;
        }

        public IBoardSlot GetDestination(IBoardSlot selectedDogSlot)
        {
            var dogNeighbors = selectedDogSlot.GetValidNeighbors();
            var dogPossibleNeighbors = dogNeighbors.Where(n => n.IsSlotEmpty()).ToArray();

            var randomSkip = Random.Range(0, dogPossibleNeighbors.Count());
            var dogNeighbor = dogPossibleNeighbors.Skip(randomSkip).First();

            return dogNeighbor;
        }

    }
}
