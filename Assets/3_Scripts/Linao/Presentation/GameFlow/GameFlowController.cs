using System;
using System.Collections;
using KurGames.Mapuches.Common.Presentation.GameFlow;
using KurGames.Mapuches.Linao.Domain.GameFlow;
using KurGames.Mapuches.Linao.Domain.Player;
using KurGames.Mapuches.Linao.Presentation.Player;
using UnityEngine;

namespace KurGames.Mapuches.Linao.Presentation.GameFlow
{
    public class GameFlowController : BaseGameFlowController
    {
        [SerializeField] private int scoreDiffToWin = 4;

        protected override void Awake()
        {
            base.Awake();
        }

        public override void StartGame(int id = 1)
        {
            //bool playerIsMaw = id == 1; // If it's not Maw, it's Millán
            //var gamePlayerMaw = new GamePlayer(isHumanPlayer: playerIsMaw, speed: mawSpeed, playerType: PlayerType.Maw);
            //mawController.SetPlayer(gamePlayerMaw);

            //var gamePlayerMillan = new GamePlayer(isHumanPlayer: !playerIsMaw, speed: millanSpeed, playerType: PlayerType.Millan);
            //millanController.SetPlayer(gamePlayerMillan, mawController);

            //var p1 = mawController.GetPlayer();
            //var p2 = millanController.GetPlayer();
            //GameFlowService.SetPlayers(new[] { p1, p2 });

            Debug.Log("Linao: Start game!");
            GameFlowService.StartGame(scoreDiffToWin);
        }
    }
}
