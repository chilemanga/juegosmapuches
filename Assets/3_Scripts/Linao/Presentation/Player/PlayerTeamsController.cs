using UnityEngine;
using KurGames.Mapuches.Linao.Domain.Player;
using KurGames.Mapuches.Linao.Presentation.GameFlow;
using KurGames.Mapuches.Linao.Domain.GameFlow;
using KurGames.Mapuches.Linao.Presentation.Player;
using System.Collections.Generic;

namespace KurGames.Mapuches.Linao.Presentation.Player
{
    public class PlayerTeamsController : MonoBehaviour
    {
        [SerializeField] private List<LinaoPlayerController> homeTeamPlayers = default;
        [SerializeField] private List<LinaoPlayerController> awayTeamPlayers = default;
        [SerializeField] private float playersSpeed = 2f;

        private bool gameStarted = false;
        private List<Vector3> homeTeamStartPositions = new List<Vector3>();
        private List<Vector3> awayTeamStartPositions = new List<Vector3>();
        private LinaoPlayerController currentControlledPlayer = null;

        private void Awake()
        {
            SetupTeams();
            GameFlowService.OnGameStart += OnGameStart;
            GameFlowService.OnGoal += HandleGoal;
            GameFlowService.OnGameFinish += OnGameFinish;
        }

        private void OnDestroy()
        {
            GameFlowService.OnGameStart -= OnGameStart;
            GameFlowService.OnGameFinish -= OnGameFinish;
        }

        private void SetupTeams()
        {
            homeTeamStartPositions.Clear();
            awayTeamStartPositions.Clear();

            for (int i = 0; i < homeTeamPlayers.Count; i++)
            {
                var player = homeTeamPlayers[i];
                // TODO: hardcoding the first team player as the one controlled by input
                bool isControlled = i == 0;
                bool isGoalKeeper = i == homeTeamPlayers.Count - 1;
                Debug.Log($"i={i}, goalkeeper={isGoalKeeper}");
                player.SetPlayer(new GamePlayer(true, isControlled ? playersSpeed : 1f, isGoalKeeper), isControlled);
                if (isControlled)
                {
                    currentControlledPlayer = player;
                }
                homeTeamStartPositions.Add(player.transform.position);
            }
            int j = 0;
            foreach (var opponent in awayTeamPlayers)
            {
                bool isGoalKeeper = j == awayTeamPlayers.Count - 1;
                opponent.SetPlayer(new GamePlayer(false, 1.1f, isGoalKeeper), false);
                // All the opponents follow the controlled one
                opponent.SetOpponent(currentControlledPlayer.gameObject);
                awayTeamStartPositions.Add(opponent.transform.position);
                j++;
            }

            // Set the home team opponents by its index
            int index = 0;
            foreach (var homePlayer in homeTeamPlayers)
            {
                homePlayer.SetOpponent(awayTeamPlayers[index++].gameObject);
            }
        }

        private void OnGameStart()
        {
            gameStarted = true;
        }
        private void OnGameFinish(GamePlayer winner)
        {
            gameStarted = false;
        }

        private void HandleGoal(bool wasHomePlayerGoal)
        {
            Invoke(nameof(ResetPositions), 1.5f);
        }

        private void ResetPositions()
        {
            for (int i = 0; i < homeTeamStartPositions.Count; i++)
            {
                var p = homeTeamStartPositions[i];
                homeTeamPlayers[i].transform.position = p;
            }
            for (int j = 0; j < awayTeamStartPositions.Count; j++)
            {
                var p = awayTeamStartPositions[j];
                awayTeamPlayers[j].transform.position = p;
            }
        }
    }
}
