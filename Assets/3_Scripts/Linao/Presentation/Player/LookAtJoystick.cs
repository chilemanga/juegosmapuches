using UnityEngine;
using System;

namespace KurGames.Mapuches.Linao.Presentation.Player
{
    public class LookAtJoystick : MonoBehaviour
    {
        public bool IsEnabled = default;
        [SerializeField] private Transform objectToTurn = default;
        [SerializeField] private string joystickName = "Movement";
        [SerializeField] private float minMovementThreshold = 0.01f;

        private Camera mainCamera = null;

        private void Awake()
        {
            mainCamera = Camera.main;
        }

        private void Twist(float h1, float v1)
        {
            if (h1 == 0f && v1 == 0f)
            {   // this statement allows it to recenter once the inputs are at zero 
                Vector3 curRot = objectToTurn.localEulerAngles; // the object you are rotating
                Vector3 homeRot;
                if (curRot.y > 180f)
                {   // this section determines the direction it returns home 
                    homeRot = new Vector3(0f, 359.999f, 0f); //it doesnt return to perfect zero 
                }
                else
                {   // otherwise it rotates wrong direction 
                    homeRot = Vector3.zero;
                }
                objectToTurn.localEulerAngles = Vector3.Slerp(curRot, homeRot, Time.deltaTime * 2);
            }
            else
            {
                objectToTurn.localEulerAngles = new Vector3(0f, Mathf.Atan2(h1, v1) * 180 / Mathf.PI, 0f); // this does the actual rotation according to inputs
            }
        }

        private void Update()
        {
            if (!IsEnabled || UltimateJoystick.GetDistance(joystickName) <= minMovementThreshold)
            {
                return;
            }
            float h = UltimateJoystick.GetHorizontalAxis(joystickName);
            float v = UltimateJoystick.GetVerticalAxis(joystickName);
            Twist(h, v);
        }
    }
}
