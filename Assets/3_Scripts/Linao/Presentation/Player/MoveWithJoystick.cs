using UnityEngine;
using System;

namespace KurGames.Mapuches.Linao.Presentation.Player
{
    public class MoveWithJoystick : MonoBehaviour
    {
        public bool IsEnabled = default;
        [SerializeField] private Transform objectToMove = default;
        public float Speed = 2f;
        [SerializeField] private string joystickName = "Movement";
        [SerializeField] private float minMovementThreshold = 0.01f;

        private bool _isMoving = false;
        public bool IsMoving
        {
            get
            {
                return _isMoving;
            }
            set
            {
                _isMoving = value;
                if (_isMoving)
                {
                    this.onMovement?.Invoke();
                }
                else
                {
                    this.onStopMovement?.Invoke();
                }
            }
        }

        private Action onMovement;
        private Action onStopMovement;

        public void SetOnMovement(Action onMovement)
        {
            this.onMovement = onMovement;
        }
        public void SetOnStopMovement(Action onStopMovement)
        {
            this.onStopMovement = onStopMovement;
        }

        public void Move(float h, float v)
        {
            float magnitude = Speed * Time.deltaTime;
            float xAdd = objectToMove.position.x + (h * magnitude);
            float zAdd = objectToMove.position.z + (v * magnitude);
            objectToMove.position = new Vector3(xAdd, objectToMove.position.y, zAdd);

            IsMoving = true;
        }

        public void MoveForward()
        {
            float magnitude = Speed * Time.deltaTime;
            objectToMove.Translate(Vector3.forward * magnitude);

            IsMoving = true;
        }

        private void Update()
        {
            if (!IsEnabled || UltimateJoystick.GetDistance(joystickName) <= minMovementThreshold)
            {
                IsMoving = false;
                return;
            }
            Move(UltimateJoystick.GetHorizontalAxis(joystickName), UltimateJoystick.GetVerticalAxis(joystickName));
        }
    }
}
