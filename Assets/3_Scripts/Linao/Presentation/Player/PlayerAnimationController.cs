using UnityEngine;
using System;

namespace KurGames.Mapuches.Linao.Presentation.Player
{
    public class PlayerAnimationController : MonoBehaviour
    {
        [SerializeField] private Animator animator = default;
        public string runKey = "run";
        public string jumpKey = "jump";
        public string stopKey = "stop";
        public string shootKey = "shoot";
        public string fallKey = "fall";

        public void Run()
        {
            ExecuteAnimation(runKey);
        }
        public void Stop()
        {
            ExecuteAnimation(stopKey);
        }
        public void Jump()
        {
            ExecuteAnimation(jumpKey);
        }
        public void Fall()
        {
            ExecuteAnimation(fallKey);
        }
        public void ExecuteAnimation(string triggerName)
        {
            animator.SetTrigger(triggerName);
        }

#if UNITY_EDITOR
        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.W))
            {
                Run();
            }
            if (Input.GetKeyDown(KeyCode.S))
            {
                Stop();
            }
            if (Input.GetKeyDown(KeyCode.Space))
            {
                Jump();
            }
            if (Input.GetKeyDown(KeyCode.F))
            {
                Fall();
            }
        }
#endif
    }
}
