using UnityEngine;
using KurGames.Mapuches.Linao.Domain.GameFlow;

namespace KurGames.Mapuches.Linao.Presentation.Player
{
    public class GoalController : MonoBehaviour
    {
        [SerializeField] private bool isHomeGoal = default;
        [SerializeField] private ScoreController scoreController = default;

        private void OnTriggerEnter(Collider other)
        {
            var player = other.GetComponentInParent<LinaoPlayerController>();
            if (player == null) return;
            // TODO: Check ball, not player
            if (player.IsControlledByInput && !isHomeGoal)
            {
                // TODO: don't use score controller reference. GameFlowService must trigger the event
                scoreController.AddScoreToPlayer();
                GameFlowService.Goal(true);
            }
            //else
            //{
            //    scoreController.AddScoreToOpponent();
            //}
        }
    }
}
