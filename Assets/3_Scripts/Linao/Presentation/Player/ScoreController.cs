using KurGames.Mapuches.Linao.Domain.GameFlow;
using UnityEngine;
using TMPro;
using KurGames.Mapuches.Linao.Domain.Player;

namespace KurGames.Mapuches.Linao.Presentation.Player
{
    public class ScoreController : MonoBehaviour
    {
        [SerializeField] private bool isPlayer1 = default;
        [SerializeField] private TextMeshProUGUI scorePlayerValue = default;
        [SerializeField] private TextMeshProUGUI scoreOpponentValue = default;

        private int playerScore = default;
        private int opponentScore = default;

        private void Awake()
        {
            GameFlowService.OnGameStart += OnGameStart;
        }

        private void OnDestroy()
        {
            GameFlowService.OnGameStart -= OnGameStart;
        }

        private void OnGameStart()
        {
            UpdateScore((int)default, (int)default);
        }

        public void UpdateScore(int playerScore, int opponentScore)
        {
            this.playerScore = playerScore;
            this.opponentScore = opponentScore;
            scorePlayerValue.text = playerScore.ToString();
            scoreOpponentValue.text = opponentScore.ToString();
        }

        public void AddScoreToPlayer()
        {
            UpdateScore(this.playerScore + 1, this.opponentScore);
        }
        public void AddScoreToOpponent()
        {
            UpdateScore(this.playerScore, this.opponentScore + 1);
        }
    }
}
