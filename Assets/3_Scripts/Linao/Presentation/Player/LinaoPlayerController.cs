using UnityEngine;
using KurGames.Mapuches.Linao.Domain.Player;
using KurGames.Mapuches.Linao.Presentation.GameFlow;
using KurGames.Mapuches.Linao.Domain.GameFlow;
using KurGames.Mapuches.Linao.Presentation.Player;

namespace KurGames.Mapuches.Linao.Presentation.Player
{
    public class LinaoPlayerController : MonoBehaviour
    {
        [SerializeField] private MoveWithJoystick movementBehaviour = default;
        [SerializeField] private LookAtJoystick lookAtBehaviour = default;
        [SerializeField] private PlayerAnimationController animationController = default;
        [SerializeField] private GameObject cursor = default;
        // FIXME: Adding a hardcoded ball
        [SerializeField] private GameObject ball = default;

        private GamePlayer gamePlayer = null;
        private GameObject opponent = null;
        private Camera mainCamera = null;
        private bool gameStarted = false;
        public bool IsControlledByInput = false;

        private void Awake()
        {
            mainCamera = Camera.main;
            GameFlowService.OnGameStart += OnGameStart;
            GameFlowService.OnGameFinish += OnGameFinish;
            movementBehaviour.SetOnMovement(Run);
            movementBehaviour.SetOnStopMovement(Stop);
        }

        private void OnDestroy()
        {
            GameFlowService.OnGameStart -= OnGameStart;
            GameFlowService.OnGameFinish -= OnGameFinish;
        }

        public void Run()
        {
            if (!gameStarted)
            {
                return;
            }
            animationController.Run();
        }
        public void Stop()
        {
            if (!gameStarted)
            {
                return;
            }
            animationController.Stop();
        }
        public void Shoot()
        {
            if (!gameStarted)
            {
                return;
            }
            animationController.Jump();
        }

        public void SetIsControlledByInput(bool isControlled)
        {
            this.IsControlledByInput = isControlled;
            bool inputControlled = gamePlayer.IsHumanPlayer && this.IsControlledByInput;
            movementBehaviour.IsEnabled = inputControlled;
            lookAtBehaviour.IsEnabled = inputControlled;
            cursor.SetActive(isControlled);
            if (ball != null)
            {
                ball.SetActive(isControlled);
            }
        }

        public void SetPlayer(GamePlayer player, bool isControlledByInput = false)
        {
            gamePlayer = player;
            movementBehaviour.Speed = gamePlayer.Speed;
            SetIsControlledByInput(isControlledByInput);
        }

        public void SetOpponent(GameObject opponent)
        {
            this.opponent = opponent;
        }

        private void OnGameStart()
        {
            Invoke(nameof(SetRealOnGameStart), 2f);
        }
        private void SetRealOnGameStart()
        {
            gameStarted = true;
        }
        private void OnGameFinish(GamePlayer winner)
        {
            gameStarted = false;
        }

        public GamePlayer GetPlayer()
        {
            return gamePlayer;
        }

        // HACK: A quick way of following something
        private void Update()
        {
            if (!gameStarted)
            {
                return;
            }
            if (!IsControlledByInput)
            {
                // Goalkeeper just looks, doesn't move
                transform.LookAt(opponent.transform);
                if (gamePlayer.IsHumanPlayer)
                {
                    if (!gamePlayer.IsGoalKeeper)
                    {
                        // Check distance to opponent
                        var opponentDistance = Vector3.Distance(transform.position, opponent.transform.position);
                        if (opponentDistance > 1f)
                        {
                            movementBehaviour.MoveForward();
                        }
                    }
                }
                else
                {
                    // non human team always follows the player with the ball
                    if (!gamePlayer.IsGoalKeeper)
                    {
                        movementBehaviour.MoveForward();
                    }
                }
            }
        }
    }
}
