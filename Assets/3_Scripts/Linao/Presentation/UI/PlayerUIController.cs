﻿using KurGames.Mapuches.Common.Presentation.UI;
using KurGames.Mapuches.Linao.Domain.GameFlow;
using KurGames.Mapuches.Linao.Domain.Player;
using UnityEngine;
using TMPro;

namespace KurGames.Mapuches.Linao.Presentation.UI
{
    public class PlayerUIController : MonoBehaviour
    {
        [SerializeField] private Canvas canvas;

        private void Awake()
        {
            canvas.enabled = false;
            GameFlowService.OnGameStart += OnGameStart;
            GameFlowService.OnGameFinish += OnGameFinish;
        }

        private void OnGameStart()
        {
            canvas.enabled = true;
        }

        private void OnGameFinish(GamePlayer gamePlayer)
        {
            canvas.enabled = false;
        }
    }
}