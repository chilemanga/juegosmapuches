﻿using KurGames.Mapuches.Common.Presentation.UI;
using KurGames.Mapuches.Linao.Domain.GameFlow;
using KurGames.Mapuches.Linao.Domain.Player;

namespace KurGames.Mapuches.Linao.Presentation.UI
{
    public class PopupManager : BasePopupManager
    {
        protected override void Awake()
        {
            base.Awake();
            GameFlowService.OnGameFinish += OnGameFinish;
        }

        private void OnGameFinish(GamePlayer gamePlayer)
        {
            base.OnGameFinish(gamePlayer.IsHumanPlayer);
        }
    }
}