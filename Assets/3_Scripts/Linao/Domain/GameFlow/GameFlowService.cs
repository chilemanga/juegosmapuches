using System;
using System.Collections.Generic;
using System.Linq;
using KurGames.Mapuches.Linao.Domain.Player;
using UnityEngine;
using _3_Scripts.Common.Infrastructure.CoroutineRunner;

namespace KurGames.Mapuches.Linao.Domain.GameFlow
{
    // TODO; use zenject instead of an static class...
    public static class GameFlowService
    {
        private static List<GamePlayer> players = new List<GamePlayer>();
        public static event Action OnGameStart = delegate { };
        public static event Action<bool> OnGoal = delegate { };
        public static event Action<GamePlayer> OnGameFinish = delegate { };

        public static int playerScore = 0;
        public static int opponentScore = 0;
        private static int winDifference = 4;

        public static void SetPlayers(IEnumerable<GamePlayer> newPlayers)
        {
            Debug.Log("<color=orange>GameFlowService: SetPlayers</color>");
            players.Clear();

            foreach (var player in newPlayers)
            {
                Debug.Log($"GameFlowService: Add Player: [<color=cyan>isHuman:{player.IsHumanPlayer}</color>]");
                players.Add(player);
                if (player.IsHumanPlayer)
                {

                }
            }
        }

        public static void StartGame(int scoreDiffToWin)
        {
            Debug.Log($"<color=orange>GameFlowService: StartGame, scoreToWin; [{scoreDiffToWin}]</color>");

            foreach (var gamePlayer in players)
            {
                gamePlayer.Restart();
            }

            winDifference = scoreDiffToWin;
            OnGameStart?.Invoke();
        }
        public static void Goal(bool isHomePlayerGoal)
        {
            if (isHomePlayerGoal)
            {
                playerScore++;
            }
            else
            {
                opponentScore++;
            }
            Debug.Log($"<color=yellow>GameFlowService: Goal from player? [<color=cyan>isHuman:{isHomePlayerGoal}</color>] Score: P1 {playerScore} - CPU {opponentScore}</color>");

            if (playerScore - opponentScore >= winDifference)
            {
                FinishGame(new GamePlayer(true));
                return;
            }
            if (opponentScore - playerScore >= winDifference)
            {
                FinishGame(new GamePlayer(false));
                return;
            }

            OnGoal?.Invoke(isHomePlayerGoal);
        }

        public static void FinishGame(GamePlayer winner)
        {
            Debug.Log($"<color=yellow>GameFlowService: FinishGame winner: [<color=cyan>isHuman:{winner.IsHumanPlayer}</color>]</color>");
            OnGameFinish?.Invoke(winner);
        }
    }
}
