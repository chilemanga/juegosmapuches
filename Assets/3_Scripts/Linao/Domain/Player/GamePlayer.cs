namespace KurGames.Mapuches.Linao.Domain.Player
{
    public class GamePlayer
    {
        public bool IsHumanPlayer { get; private set; }
        public float Speed { get; private set; }
        public bool IsGoalKeeper { get; private set; }

        public GamePlayer(bool isHumanPlayer, float speed = 2f, bool isGoalKeeper = false)
        {
            IsHumanPlayer = isHumanPlayer;
            Speed = speed;
            IsGoalKeeper = isGoalKeeper;
            Restart();
        }

        public void Restart()
        {
        }
    }
}
